<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $file = __DIR__ . $_SERVER['REQUEST_URI'];
    if (is_file($file)) {
        return false;
    }
}

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//Requiring functions and middlewares
require 'includes/config.php';
require 'includes/user_controller.php';
require 'includes/network_controller.php';
require 'includes/node_controller.php';
require 'includes/data_controller.php';
require 'includes/trigger_controller.php';
require 'includes/listener_controller.php';
require 'middleware/Auth.php';

require __DIR__ . '/../vendor/autoload.php';

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

//Add Middlewares
$app->add(new Auth($container));

$app->group('/utils', function () use ($app) {
    $app->get('/time', function ($request, $response) {
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        //echo file_get_contents( "scripts/mail.py" );
        return $response->getBody()->write(date('Y-m-d\TH:i:sO'));
    });
});

$app->group('/user', function () use ($app) {
    $app->get('/logout', function ($request, $response) {
        $path = $request->getUri()->getPath();
        $success = logoutUser();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = array('success' => true);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $app->get('/authState', function ($request, $response) {
        $input = $request->getParsedBody();
        $path = $request->getUri()->getPath();
        $result = retrieveUser($this->db);
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        return $response->withJson($result);
    });
    $this->post('/checkToken', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = verifyToken($this->db, $input['UUID'], $input['token']);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = array('success' => true);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/signup', function (Request $request, Response $response) {
        $input = $request->getParsedBody()['user'];
        $success = createAccount($this->db, $input['email'], $input['password'], $input['firstName'], $input['lastName']);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = array('success' => true);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/signin', function (Request $request, Response $response) {
        $input = $request->getParsedBody()['user'];
        $success = signin($this->db, $input['email'], $input['password'], $input['remember']);
        if ($success) {
            $result = array('success' => true, 'token' => $_SESSION['token'], 'UUID' => $_SESSION['uuid']);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        return $response->withJson($result);
    });
    $this->group('/update', function () use ($app) {
        $this->post('/password', function (Request $request, Response $response) {
            $input = $request->getParsedBody();
            $success = modifyPassword($this->db, $input['password'], $input['newPassword']);
            if ($success) {
                $result = array('success' => true, 'message' => "Password changed successfully. Please authenticate again.");
            } else {
                $result = array('success' => false, 'reason' => $_SESSION['error']);
            }
            $path = $request->getUri()->getPath();
            $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
            return $response->withJson($result);
        });
    });
});

$app->group('/register', function () use ($app) {
    $this->post('/network', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = registerNetwork($this->db, $input['networkID'], $input['name'], $input['description']);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = array('success' => true);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/node', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = registerNode($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = array('success' => true);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/trigger', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = registerTrigger($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = array('success' => true);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/listener', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = registerListener($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = array('success' => true);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
});

$app->group('/view', function () use ($app) {
    $this->post('/network', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $output = viewNetworks($this->db);
        if ($output['success']) {
            $result = array('success' => true, 'count' => $output['count'], 'networks' => $output['networks']);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        return $response->withJson($result);
    });
    $this->post('/node', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $output = viewNodes($this->db, $input['networkID']);
        if ($output['success']) {
            $result = array('success' => true, 'count' => $output['count'], 'nodes' => $output['nodes']);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        return $response->withJson($result);
    });
    $this->post('/trigger', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $output = viewTriggers($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($output['success']) {
            $result = array('success' => true, 'count' => $output['count'], 'triggers' => $output['triggers']);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/listener', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $output = viewListener($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($output['success']) {
            $result = array('success' => true, 'count' => $output['count'], 'listeners' => $output['listeners']);
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
});

$app->group('/update', function () use ($app) {
    $this->group('/network', function () use ($app) {
        $this->post('/key', function (Request $request, Response $response) {
            $input = $request->getParsedBody();
            $success = updateKey($this->db, $input['networkID']);
            if ($success) {
                $result = array('success' => true, 'message' => "Publish key changed successfully.");
            } else {
                $result = array('success' => false, 'reason' => $_SESSION['error']);
            }
            $path = $request->getUri()->getPath();
            $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
            return $response->withJson($result);
        });
    });
});

$app->post('/sync', function (Request $request, Response $response) {
    $input = $request->getParsedBody();

    $success = dataSync($this->db, $input);
    $path = $request->getUri()->getPath();
    $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
    if ($success) {
        $result = array('success' => true);
    } else {
        $result = array('success' => false, 'reason' => $_SESSION['error']);
    }
    return $response->withJson($result);
});

$app->group('/get', function () use ($app) {
    $this->post('/nodeData', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = getNodeData($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/nodeDataTypes', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        echo "here";
        $success = getNodeTypes($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/nodeDataT', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = getNodeDataT($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/networkData', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = getNetworkData($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/networkDataT', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = getNetworkDataT($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/userData', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = getUserData($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/userDataT', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = getUserDataT($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
});

$app->group('/data', function () use ($app) {
    $this->post('/node', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = viewNodeData($this->db, $input);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/network', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = viewNetworkData($this->db, $input['networkID']);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
    $this->post('/user', function (Request $request, Response $response) {
        $input = $request->getParsedBody();
        $success = viewUserData($this->db);
        $path = $request->getUri()->getPath();
        $this->logger->addInfo("Conenction from $_SERVER[REMOTE_ADDR]. URL: $path");
        if ($success) {
            $result = $_SESSION['data'];
        } else {
            $result = array('success' => false, 'reason' => $_SESSION['error']);
        }
        return $response->withJson($result);
    });
});

// Run app
$app->run();