<?php
function registerListener($db, $input) {
    $networkID = $input['networkID'];
    $nodeID = $input['nodeID'];
    $script = $input['script'];
    try {
        $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID, 'nodeID' => $nodeID, 'script' => $script];
        $node = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) RETURN n", $params)->getRecords();
        $listener = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(Network{networkID:{networkID}})-[:CONNECTED*]->(:Node{nodeID:{nodeID}})-[:ATTACHED]->(n:Listener{script:{script}}) RETURN n", $params)->getRecords();
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    if (empty($node)) {
        $_SESSION['error'] = "Network or node does not exist.";
        return false;
    }

    if (!empty($listener)) {
        $_SESSION['error'] = "One listener already exists for this node, maximum 1 allowed.";
        return false;
    }else {
        try {
            $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(ne:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}})
                CREATE (l:Listener {script:{script}}) CREATE (n)-[:ATTACHED]->(l)", $params);
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
        return true;
    }
}

function viewListener($db, $input) {
    $networkID = $input['networkID'];
    $nodeID = $input['nodeID'];
    try {
        $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID, 'nodeID' => $nodeID];
        $result = $db->run("MATCH (:User {uuid:{uuid}})-[:OWNS]->(:Network {networkID:{networkID}})-[:CONNECTED*]->(:Node{nodeID:{nodeID}})-[:ATTACHED]->(n:Listener) RETURN n", $params)->getRecords();
        $listeners = [];
        foreach ($result as $record) {
            $listeners[] = $record->get('n')->values();
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        $output = array('success' => false);
        return $output;
    }
    if (empty($listeners)){
        $_SESSION['error'] = "No listeners are found!";
        $output = array('success' => false);
    } else {
        $count = count($listeners);
        $output = array('success' => true, 'count' => $count, 'listeners' => $listeners);
    }
    return $output;
}