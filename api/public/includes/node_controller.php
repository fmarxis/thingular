<?php
function registerNode($db, $input) {
    $networkID = $input['networkID'];
    $nodeID = $input['nodeID'];
    $name = $input['name'];
    $description = $input['description'];
    $lon = $input['lon'];
    $lat = $input['lat'];
    $lastSyncTime = $input['lastSyncTime'];
    $inactivityLimit = $input['inactivityLimit'];
    $connectedTo = $input['connectedTo'];
        try {
            $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID, 'nodeID' => $nodeID, 'name' => $name,
                'description' => $description, 'lon' => $lon, 'lat' => $lat, 'lastSyncTime' => $lastSyncTime, 'inactivityLimit'=>$inactivityLimit,
                'connectedTo' => $connectedTo, "dataType" => json_encode(array())];
            $network = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(n:Network{networkID:{networkID}}) RETURN n", $params)->getRecords();
            $node = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) RETURN n", $params)->getRecords();
            $nodeTo = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{connectedTo}}) RETURN n", $params)->getRecords();
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
    }

    if (empty($network)) {
        $_SESSION['error'] = "Network does not exist.";
        return false;
    }

    if (!empty($node)) {
        $_SESSION['error'] = "Node ID already exists.";
        return false;
    }else {
        try {
            if((int)$connectedTo == -1) {
                $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(ne:Network{networkID:{networkID}})
                    CREATE (no:Node {nodeID:{nodeID}, name:{name}, description:{description}, lon:{lon}, lat:{lat}, dataType:[],
                    lastSyncTime:{lastSyncTime}, inactivityLimit:{inactivityLimit}}) CREATE (ne)-[:CONNECTED]->(no)", $params);
            } else {
                if (empty($nodeTo)) {
                    $_SESSION['error'] = "Target node does not exist.";
                    return false;
                }
                $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(Network{networkID:{networkID}})-[:CONNECTED*]->(n1:Node{nodeID:{connectedTo}})
                    CREATE (n2:Node {nodeID:{nodeID}, name:{name}, description:{description}, lon:{lon}, lat:{lat}, dataType:[],
                    lastSyncTime:{lastSyncTime}, inactivityLimit:{inactivityLimit}}) CREATE (n1)-[:CONNECTED]->(n2)", $params);
            }
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
        return true;
    }
}

function viewNodes($db, $networkID) {
    try {
        $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID];
        $result = $db->run("MATCH (:User {uuid:{uuid}})-[:OWNS]->(:Network {networkID:{networkID}})-[:CONNECTED*]->(n:Node) RETURN n", $params)->getRecords();
        $nodes = [];
        foreach ($result as $record) {
            $nodes[] = $record->get('n')->values();
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        $output = array('success' => false);
        return $output;
    }
    if (empty($nodes)){
        $_SESSION['error'] = "No nodes are found!";
        $output = array('success' => false);
    } else {
        $count = count($nodes);
        $output = array('success' => true, 'count' => $count, 'nodes' => $nodes);
    }
    return $output;
}