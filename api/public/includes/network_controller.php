<?php
function registerNetwork($db, $networkID, $name, $description) {
    try {
        $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID];
        $network = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(n:Network{networkID:{networkID}}) RETURN n", $params)->getRecords();
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    if (!empty($network)) {
        $_SESSION['error'] = "Network ID already exists.";
    }else {
        $key = sha1($_SESSION['uuid'] . uniqid());
        $params = ['key' => $key];
        $result = $db->run("MATCH (n:Network{pubKey:{key}}) RETURN n", $params)->getRecords();
        while (count($result) > 1) {
            $key = sha1($_SESSION['uuid'] . uniqid());
            $params = ['key' => $key];
            $result = $db->run("MATCH (n:Network{pubKey:{key}}) RETURN n", $params)->getRecords();
        }
        try {
            $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID, 'name' => $name, 'description' => $description, 'key' => $key];
            $result = $db->run("MATCH (u:User{uuid:{uuid}}) CREATE (n:Network {networkID:{networkID}, name:{name}, 
                description:{description}, pubKey:{key}}) CREATE (u)-[:OWNS]->(n)", $params);
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
        return true;
    }
}

function viewNetworks($db) {
    try {
        $params = ['uuid' => $_SESSION['uuid']];
        $result = $db->run("MATCH (:User {uuid:{uuid}})-[:OWNS]->(n:Network) RETURN n", $params)->getRecords();
        $networks = [];
        foreach ($result as $record) {
            $networks[] = $record->get('n')->values();
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        $output = array('success' => false);
        return $output;
    }
    if (empty($networks)){
        $_SESSION['error'] = "No networks are found!";
        $output = array('success' => false);
    } else {
        $count = count($networks);
        $output = array('success' => true, 'count' => $count, 'networks' => $networks);
    }
    return $output;
}

function updateKey($db, $networkID) {
    // See if the network exists
    try {
        $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID];
        $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(n:Network{networkID:{networkID}}) RETURN n", $params)->getRecord();
    } catch (PDOException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    if (empty($result)){
        $_SESSION['error'] = "Network doesn't exist.";
        return false;
    } else {
        try {
            $key = sha1($_SESSION['uuid'] . uniqid());
            $params = ['key' => $key];
            $result = $db->run("MATCH (n:Network{pubKey:{key}}) RETURN n", $params)->getRecords();
            while (count($result) > 1) {
                $key = sha1($_SESSION['uuid'] . uniqid());
                $result = $db->run("MATCH (n:Network{pubKey:{key}}) RETURN n", $params)->getRecords();
            }
            $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID, 'key' => $key];
            $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(n:Network{networkID:{networkID}}) SET n.pubKey = {key} RETURN n", $params);
        } catch (PDOException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
        return true;
    }

    return false;
}