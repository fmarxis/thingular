<?php

function verifyNameSpace($uuid) {
    return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
        '[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}

//Generator for Version 5 UUID
function uuid_generator($namespace, $name)
{
    if(!verifyNameSpace($namespace)) return false;
    // Get hexadecimal components of namespace
    $nhex = str_replace(array('-','{','}'), '', $namespace);
    // Binary Value
    $nstr = '';
    // Convert Namespace UUID to bits
    for($i = 0; $i < strlen($nhex); $i+=2)
    {
        $nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    }
    // Calculate hash value
    $hash = sha1($nstr . $name);
    return sprintf('%08s-%04s-%04x-%04x-%12s',
        // 32 bits for "time_low"
        substr($hash, 0, 8),
        // 16 bits for "time_mid"
        substr($hash, 8, 4),
        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 5
        (hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,
        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        (hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
        // 48 bits for "node"
        substr($hash, 20, 12)
    );
}



//Create Account with String pEmail and pPassword
function createAccount($db, $pEmail, $pPassword, $firstName, $lastName) {
    // First check we have data passed in.
    if (!empty($pEmail) && !empty($pPassword)) {
        $uLen = strlen($pEmail);
        $pLen = strlen($pPassword);
        $fnLen = strlen($firstName);
        $lnLen = strlen($lastName);

        try {
            $params = ['email' => $pEmail];
            $user = $db->run("MATCH (u:User {email:{email}}) RETURN u", $params)->getRecords();
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }

        // Error checks
        if ($uLen <= 5 || $uLen >= 32) {
            $_SESSION['error'] = "Email must be between 4 and 30 characters.";
        }elseif ($pLen < 6) {
            $_SESSION['error'] = "Password must be longer then 6 characters.";
        }elseif ($fnLen > 64) {
            $_SESSION['error'] = "First name is too long.";
        }elseif ($lnLen > 64) {
            $_SESSION['error'] = "Last name is too long.";
        }elseif (!empty($user)) {
            $_SESSION['error'] = "Email already exists.";
        }else {
            $uuid = uuid_generator("6c01f579-b7be-47f9-8ac6-cccf6f0b5d12",$pEmail);
            $params = ['uuid' => $uuid];
            $result = $db->run("MATCH (u:User{uuid:{uuid}}) RETURN u", $params)->getRecords();
            while (count($result) > 1) {
                $uuid = uuid_generator("6c01f579-b7be-47f9-8ac6-cccf6f0b5d12",$pEmail);
                $params = ['uuid' => $uuid];
                $result = $db->run("MATCH (u:User{uuid:{uuid}}) RETURN u", $params)->getRecords();
            }
            $subKey = uuid_generator("6c01f579-b7be-47f9-8ac6-cccf6f0b5d12",uniqid());
            $params = ['subKey' => $subKey];
            $result = $db->run("MATCH (u:User{subKey:{subKey}}) RETURN u", $params)->getRecords();
            while (count($result) > 1) {
                $subKey = uuid_generator("6c01f579-b7be-47f9-8ac6-cccf6f0b5d12",uniqid());
                $params = ['subKey' => $subKey];
                $result = $db->run("MATCH (u:User{subKey:{subKey}}) RETURN u", $params)->getRecords();
            }
            $hash = password_hash($pPassword, PASSWORD_DEFAULT);
            $time = date(DateTime::ISO8601);
            $TTL = "0";
            $token = hash('sha256', $uuid . uniqid());
            //insert user info into database with hashed password
            try {
                $params = ['email' => $pEmail, 'uuid' => $uuid, 'hash' => $hash, 'firstName' => $firstName, 'lastName' => $lastName,
                    'token' => $token, 'subKey' => $subKey, 'TTL' => $TTL, 'time' => $time];
                $result = $db->run("CREATE (:User {uuid:{uuid}, email:{email}, password:{hash}, firstName:{firstName},
                    lastName:{lastName}, token:{token}, subKey:{subKey}, tokenTime:{time}, tokenTTL:{TTL}})", $params);
            } catch (DBException $e) {
                $_SESSION['error'] = "Query Failed!";
                return false;
            }

            return true;
        }
    }

    return false;
}

//Generate SHA256 token, valid within an hour with the same IP
function generateToken($uuid, $email, $password) {
    //date time that is accurate to the hour
    $time = date('m/d/Y h', time());
    return hash('sha256', $uuid . $email . $password . $GLOBALS['salt'] . $time. $_SERVER['REMOTE_ADDR']. uniqid());
}

//Verify token is valid and has not expired
function verifyToken($db, $uuid, $token) {
    try {
        $params = ['uuid' => $uuid];
        $user = $db->run("MATCH (u:User {uuid:{uuid}}) RETURN u", $params)->getRecords();
    } catch (PDOException $e) {
        $_SESSION['error'] = "Query Failed: " . $e->getMessage();
        return false;
    }

    if (count($user)!=1){
        $_SESSION['error'] = "User doesn't exist.";
        return false;
    } else {
        $result = $user[0]->value('u')->values();
    }

    $validToken = $result['token'];
    $tokenTTL = $result['tokenTTL'];
    if ((int)$tokenTTL == -1){
        $validTime = time() + 3600;
    } else {
        $validTime = strtotime($result['tokenTime'])+(((int)$result['tokenTTL'])*3600);
    }
    if ($validToken != $token) {
        $_SESSION['error'] = "Invalid Token";
        return false;
    } else if (time() > $validTime) {
        $_SESSION['error'] = "Token Expired";
        return false;
    } else {
        $time = date(DateTime::ISO8601);
        $params = ['uuid' => $uuid, 'time' => $time];
        $result = $db->run("MATCH (u:User {uuid:{uuid}}) SET u.tokenTime={time} RETURN u", $params);
        if (strcmp($_SERVER['SERVER_NAME'], "localhost") == 0) {
            setcookie('uuid', $uuid, time() + (86400 * 30), "/");
            setcookie('token', $token, time() + (86400 * 30), "/");
        } else {
            setcookie('uuid', $uuid, time() + (86400 * 30), "/", '.' . $_SERVER['SERVER_NAME']);
            setcookie('token', $token, time() + (86400 * 30), "/", '.' . $_SERVER['SERVER_NAME']);
        }
        return true;
    }

}

//Get a valid token using uuid
function retrieveToken($db, $uuid) {
    try {
        $params = ['uuid' => $uuid];
        $user = $db->run("MATCH (u:User {uuid:{uuid}}) RETURN u", $params)->getRecords();
    } catch (PDOException $e) {
        $_SESSION['error'] = "Query Failed: " . $e->getMessage();
        return false;
    }

    if (count($user) != 1){
        $_SESSION['error'] = "User doesn't exist.";
        return false;
    } else {
        $result = $user[0]->value('u')->values();
    }

    $validToken = $result['token'];
    if ($validToken != null) {
        return $validToken;
    } else {
        return false;
    }
}

//Retrieve User Details
function retrieveUser($db) {
    try {
        $params = ['uuid' => $_SESSION['uuid']];
        $user = $db->run("MATCH (u:User {uuid:{uuid}}) RETURN u", $params)->getRecords();
    } catch (PDOException $e) {
        $_SESSION['error'] = "Query Failed: " . $e->getMessage();
        return false;
    }

    if (count($user) != 1){
        $_SESSION['error'] = "User doesn't exist.";
        return false;
    } else {
        $result = $user[0]->value('u')->values();
        $masked = array('firstName'=>$result['firstName'],'lastName'=>$result['lastName'],
            'email'=>$result['email'], 'uuid'=>$result['uuid']);
    }

    $output = array('success' => true, 'user' => $masked);
    return $output;
}

//Logout function that removes session variables
function logoutUser() {
    // using unset will remove the variable
    // and thus logging off the user.
    unset($_SESSION['uuid']);
    unset($_SESSION['email']);
    unset($_SESSION['firstName']);
    unset($_SESSION['auth']);
    unset($_SESSION['token']);
    unset($_SESSION['data']);
    unset($_COOKIE['uuid']);
    unset($_COOKIE['token']);
    if (strcmp($_SERVER['SERVER_NAME'], "localhost") == 0) {
        setcookie('uuid', null, -1, '/');
        setcookie('token', null, -1, '/');
    } else {
        setcookie('uuid', null, -1, '/', '.' . $_SERVER['SERVER_NAME']);
        setcookie('token', null, -1, '/', '.' . $_SERVER['SERVER_NAME']);
    }
    if (session_status() == PHP_SESSION_ACTIVE) {
        session_regenerate_id();
        session_destroy();
    }
    return true;
}

//Verify username and password to login a user
function signin($db, $pEmail, $pPassword, $remember) {
    // See if the user exists
    try {
        $params = ['email' => $pEmail];
        $user = $db->run("MATCH (u:User {email:{email}}) RETURN u", $params)->getRecords();
    } catch (PDOException $e) {
        $_SESSION['error'] = "Query Failed: " . $e->getMessage();
        return false;
    }

    if (count($user)!=1){
        $_SESSION['error'] = "User doesn't exist.";
        return false;
    } else {
        $result = $user[0]->value('u')->values();
    }
    // If one row was returned, the user was logged in!
    if (password_verify($pPassword, $result['password'])) {
        logoutUser();
        session_start();
        //Prevent session hijacking from fixation
        //Replaces session id on every request
        session_regenerate_id(true);
        $_SESSION['uuid'] = $result['uuid'];
        $_SESSION['email'] = $result['email'];
        $_SESSION['firstName'] = $result['firstName'];
        $_SESSION['auth'] = true;
        $_SESSION['token'] = generateToken($result['uuid'],$result['email'],$result['password']);
        $time = date(DateTime::ISO8601);
        if($remember) {
            $TTL = -1;
        } else {
            $TTL = 1;
        }
        $params = ['uuid' => $_SESSION['uuid'], 'token' => $_SESSION['token'], 'time' => $time, 'TTL' => $TTL];
        $result = $db->run("MATCH (u:User {uuid:{uuid}}) SET u.token={token}, u.tokenTime={time}, u.tokenTTL={TTL} RETURN u", $params);
        $baseTime = time() + (86400 * 30);
        if (strcmp($_SERVER['SERVER_NAME'], "localhost") == 0) {
            setcookie('uuid', $_SESSION['uuid'], $baseTime, "/");
            setcookie('token', $_SESSION['token'], $baseTime, "/");
        } else {
            $addr = '.' . $_SERVER['SERVER_NAME'];
            setcookie('uuid', $_SESSION['uuid'], $baseTime, "/", $addr,1);
            setcookie('token', $_SESSION['token'], $baseTime, "/", $addr,1);
        }
        return true;
    } else {
        $_SESSION['error'] = "Invalid Password.";
    }

    return false;
}

//Change password in the database of the currently logged in user
function modifyPassword($db, $pass, $newPass) {
    // See if the user exists
    try {
        $params = ['uuid' => $_SESSION['uuid']];
        $user = $db->run("MATCH (u:User {uuid:{uuid}}) RETURN u", $params)->getRecords();
    } catch (PDOException $e) {
        $_SESSION['error'] = "Query Failed: " . $e->getMessage();
        return false;
    }

    if (count($user)!=1){
        $_SESSION['error'] = "User doesn't exist.";
        return false;
    } else {
        $result = $user[0]->value('u')->values();
    }

    // If one row was returned, the user was logged in!
    if (password_verify($pass, $result['password'])) {
        //original password is correct
        $hash = password_hash($newPass, PASSWORD_DEFAULT);
        try {
            $params = ['uuid' => $_SESSION['uuid'], 'hash' => $hash];
            $result = $db->run("MATCH (u:User {uuid:{uuid}}) SET u.password = {hash} RETURN u", $params);
        } catch (PDOException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
        logoutUser();
        return true;
    } else {
        $_SESSION['error'] = "Invalid Password.";
    }

    return false;
}

?>