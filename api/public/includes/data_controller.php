<?php

function validateDate($date)
{
    $d = DateTime::createFromFormat('Y-m-d\TH:i:sO', $date);
    return $d && $d->format('Y-m-d\TH:i:sO') === $date;
}

function dataSync($db, $input) {
    $enable_listener = true;
    $enable_trigger = true;
    $enable_socket = true;
    $count = count($input);
    $copy = $input;
    if($count < 3) {
        $_SESSION['error'] = "Insufficient information to sync data!";
        return false;
    } else if(!isset($input['nodeID'])) {
        $_SESSION['error'] = "nodeID is missing!";
        return false;
    } else if(!isset($input['pubKey'])) {
        $_SESSION['error'] = "Network API Key is missing!";
        return false;
    }

    $nodeID = $input['nodeID'];
    $pubKey = $input['pubKey'];
    unset($input['nodeID']);
    unset($input['pubKey']);
    $params = ['nodeID' => $nodeID, 'key' => $pubKey];
    try {
        $result = $db->run("MATCH (:Network{pubKey:{key}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) RETURN n.dataType", $params)->getRecords();
        if (count($result)!=1) {
            $_SESSION['error'] = "Network or node do not exist!";
            return false;
        } else {
            $types = $result[0]->values()[0];
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }


    if (isset($input['timeStamp'])){
        if (validateDate($input['timeStamp'])) {
            $timeStamp = $input['timeStamp'];
            unset($input['timeStamp']);
        } else {
            $timeStamp = date('Y-m-d\TH:i:sO');
            unset($input['timeStamp']);
        }
    } else {
        $timeStamp = date('Y-m-d\TH:i:sO');
    }
    if (isset($input['lon']) && isset($input['lat']) && is_numeric($input['lon']) && is_numeric($input['lat'])){
        $lon = $input['lon'];
        $lat = $input['lat'];
        unset($input['lon']);
        unset($input['lat']);
        //GPS SYNC
        $params = ['key' => $pubKey, 'nodeID' => $nodeID, 'lon' => $lon, 'lat' => $lat];
        try {
            $result = $db->run("MATCH (:Network{pubKey:{key}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) SET n.lon={lon}, n.lat={lat} RETURN n", $params);
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
    } else {
        unset($input['lon']);
        unset($input['lat']);
    }
    $datetime = DateTime::createFromFormat('Y-m-d\TH:i:sO', $timeStamp);
    $query = sprintf('MATCH (:Network{pubKey:"%s"})-[:CONNECTED*]->(n:Node{nodeID:"%s"}) 
                    MERGE (n)-[:IN_YEAR]->(y:Year{year:"%s"})
                    MERGE (y)-[:IN_MONTH]->(m:Month{month:"%s"})
                    MERGE (m)-[:IN_DAY]->(d:Day{day:"%s"})
                    MERGE (d)-[:IN_HOUR]->(h:Hour{hour:"%s"})
                    MERGE (h)-[:RECORDED]->(da:Data{time:"%s"',
                    $pubKey, $nodeID,
                    $datetime->format('Y'),
                    $datetime->format('m'),
                    $datetime->format('d'),
                    $datetime->format('H'),
                    $datetime->format('i:sO'));
    foreach($input as $key => $val) {
            $query = $query .', '.$key.':'.'"'.$val.'"';
    }
    $query = $query."})";
    try {
        $result = $db->run($query);
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $params = ['key' => $pubKey, 'nodeID' => $nodeID, 'lastSyncTime' => $timeStamp];
    try {
        $result = $db->run("MATCH (:Network{pubKey:{key}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) WHERE n.lastSyncTime < {lastSyncTime} SET n.lastSyncTime={lastSyncTime} RETURN n.dataType", $params);
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    foreach($input as $key=>$value){
        if(!in_array($key, $types)){
            array_push($types,$key);
        }
    }
    $params = ['key' => $pubKey, 'nodeID' => $nodeID, 'dataType' => $types];
    try {
        $result = $db->run("MATCH (u:User)-[:OWNS]->(:Network{pubKey:{key}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) SET n.dataType={dataType} RETURN u.uuid, u.email", $params)->getRecords()[0];
        $uuid = $result->get('u.uuid');
        $email = $result->get('u.email');
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    if ($enable_socket) {
        unset($copy['pubKey']);
        exec('node scripts/send.js "'.$uuid .'" "'.$pubKey .'" "'.json_encode($copy) .'"');
    }
    if ($enable_trigger) {
        try {
            $params = ['pubKey' => $pubKey, 'nodeID' => $nodeID];
            $result = $db->run("MATCH (:Network {pubKey:{pubKey}})-[:CONNECTED*]->(:Node{nodeID:{nodeID}})-[:ATTACHED]->(n:Trigger) RETURN n", $params)->getRecords();
            foreach ($result as $record) {
                $trigger = $record->get('n')->values();
                $trigger['dataType'];
                $trigger['script'];
                $trigger['compare'];
                $trigger['value'];
                foreach($input as $key=>$value){
                    if (strcmp($trigger['dataType'],$key) == 0) {
                        if ($trigger['compare'] == 1) {
                            if (intval($value) > intval($trigger['value'])) {
                                exec('python scripts/mail.py "'.$email.'" "'. $trigger['dataType'] .' Triggered on Node:'. $nodeID .'!" "' . json_encode($input) . '"');
                                exec('python scripts/'.$uuid.'/trigger/'.$trigger['script']);
                            }
                        } else if ($trigger['compare'] == 0) {
                            if (intval($value) == intval($trigger['value'])) {
                                exec('python scripts/mail.py "'.$email.'" "'. $trigger['dataType'] .' Triggered on Node:'. $nodeID .'!" "' . json_encode($input) . '"');
                                exec('python scripts/'.$uuid.'/trigger/'.$trigger['script']);
                            }
                        } else if ($trigger['compare'] == -1) {
                            if (intval($value) < intval($trigger['value'])) {
                                exec('python scripts/mail.py "'.$email.'" "'. $trigger['dataType'] .' Triggered on Node:'. $nodeID .'!" "' . json_encode($input) . '"');
                                exec('python scripts/'.$uuid.'/trigger/'.$trigger['script']);
                            }
                        }
                    }
                }
            }
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
    }
    if ($enable_listener) {
        try {
            $params = ['pubKey' => $pubKey, 'nodeID' => $nodeID];
            $result = $db->run("MATCH (:Network {pubKey:{pubKey}})-[:CONNECTED*]->(:Node{nodeID:{nodeID}})-[:ATTACHED]->(n:Listener) RETURN n", $params)->getRecords();
            foreach ($result as $record) {
                $listener = $record->get('n')->values();
                $listener['script'];
                exec('python scripts/mail.py "'.$email.'" "Listener on Node:'. $nodeID .'!" "' . json_encode($input) . '"');
                exec('python scripts/'.$uuid.'/listener/'.$listener['script']);
            }
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
    }
    return true;
}

function viewNodeData($db, $input) {
    $networkID = $input['networkID'];
    $nodeID = $input['nodeID'];
    $params = ['uuid' => $_SESSION['uuid'],'nodeID' => $nodeID, 'networkID' => $networkID];
    try {
        $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) RETURN n", $params)->getRecords();
        if (count($result) != 1) {
            $_SESSION['error'] = "Network or node does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    try {
        $result = $db->run('MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}})
            MATCH (n)-[:IN_YEAR]->(y:Year)-[:IN_MONTH]->(m:Month)-[:IN_DAY]->(d:Day)-[:IN_HOUR]->(h:Hour)-[:RECORDED]->(data:Data)
            RETURN y.year, m.month, d.day, h.hour, data ORDER BY y.year+m.month+d.day+h.hour+data.time DESC', $params)->getRecords();
        if (empty($result)) {
            $_SESSION['error'] = "Data does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $all_data = array();
    foreach ($result as $record) {
        $year = $record->value('y.year');
        $month = $record->value('m.month');
        $day = $record->value('d.day');
        $hour = $record->value('h.hour');
        $data = $record->value('data');
        //2016-10-12T12:31:51+1000
        $time = $year.'-'.$month.'-'.$day.'T'.$hour.':'.$data->value('time');
        $data = $data->values();
        unset($data['time']);
        $data['timeStamp'] = $time;
        array_push($all_data, $data);
    }
    $_SESSION['data'] = array('success'=> true, 'nodeID'=>$nodeID, 'data'=>$all_data);
    return true;
}

function viewNetworkData($db, $networkID) {
    $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID];
    try {
        $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(n:Network{networkID:{networkID}}) RETURN n", $params)->getRecords();
        if (count($result)!= 1) {
            $_SESSION['error'] = "Network does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    try {
        $result = $db->run('MATCH (User {uuid:{uuid}})-[:OWNS]->(Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node)
            RETURN n.nodeID', $params)->getRecords();
        if (empty($result)) {
            $_SESSION['error'] = "Network is empty!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    $network_data = [];
    foreach ($result as $record) {
        $nodeID = $record->value('n.nodeID');
        $params = ['uuid' => $_SESSION['uuid'],'nodeID' => $nodeID, 'networkID' => $networkID];
        try {
            $node_result = $db->run('MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}})
            MATCH (n)-[:IN_YEAR]->(y:Year)-[:IN_MONTH]->(m:Month)-[:IN_DAY]->(d:Day)-[:IN_HOUR]->(h:Hour)-[:RECORDED]->(data:Data)
            RETURN y.year, m.month, d.day, h.hour, data ORDER BY y.year+m.month+d.day+h.hour+data.time DESC', $params)->getRecords();
            if (empty($node_result)) {
                $node_array = array('nodeID'=>$nodeID, 'data'=>array());
                array_push($network_data, $node_array);
                continue;
            }
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
        $all_data = array();
        foreach ($node_result as $node_record) {
            $year = $node_record->value('y.year');
            $month = $node_record->value('m.month');
            $day = $node_record->value('d.day');
            $hour = $node_record->value('h.hour');
            $data = $node_record->value('data');
            //2016-10-12T12:31:51+1000
            $time = $year.'-'.$month.'-'.$day.'T'.$hour.':'.$data->value('time');
            $data = $data->values();
            unset($data['time']);
            $data['timeStamp'] = $time;
            array_push($all_data, $data);
        }
        $node_array = array('nodeID'=>$nodeID, 'data'=>$all_data);
        array_push($network_data, $node_array);
    }
    //var_dump($network_data);
    $_SESSION['data'] = array('success'=>true,'networkID'=>$networkID,'nodes'=>$network_data);
    return true;
}

function viewUserData($db) {
    $params = ['uuid' => $_SESSION['uuid']];
    try {
        $result = $db->run("MATCH (n:User {uuid:{uuid}}) RETURN n", $params)->getRecords();
        if (count($result)!=1) {
            $_SESSION['error'] = "User does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    try {
        $result = $db->run('MATCH (User {uuid:{uuid}})-[:OWNS]->(n:Network)
            RETURN n.networkID', $params)->getRecords();
        if (empty($result)) {
            $_SESSION['error'] = "User has no registered network!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $overview = [];
    foreach($result as $record) {
        $networkID = $record->value('n.networkID');
        $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID];
        try {
            $network_result = $db->run('MATCH (User {uuid:{uuid}})-[:OWNS]->(Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node)
            RETURN n.nodeID', $params)->getRecords();
            if (empty($network_result)) {
                $network_array = array('networkID'=>$networkID, 'nodes'=>array());
                array_push($overview, $network_array);
                continue;
            }
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
        $network_data = [];
        foreach ($network_result as $network_record) {
            $nodeID = $network_record->value('n.nodeID');
            $params = ['uuid' => $_SESSION['uuid'],'nodeID' => $nodeID, 'networkID' => $networkID];
            try {
                $node_result = $db->run('MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}})
            MATCH (n)-[:IN_YEAR]->(y:Year)-[:IN_MONTH]->(m:Month)-[:IN_DAY]->(d:Day)-[:IN_HOUR]->(h:Hour)-[:RECORDED]->(data:Data)
            RETURN y.year, m.month, d.day, h.hour, data ORDER BY y.year+m.month+d.day+h.hour+data.time DESC', $params)->getRecords();
                if (empty($node_result)) {
                    $node_array = array('nodeID'=>$nodeID, 'data'=>array());
                    array_push($network_data, $node_array);
                    continue;
                }
            } catch (DBException $e) {
                $_SESSION['error'] = "Query Failed!";
                return false;
            }
            $all_data = array();
            foreach ($node_result as $node_record) {
                $year = $node_record->value('y.year');
                $month = $node_record->value('m.month');
                $day = $node_record->value('d.day');
                $hour = $node_record->value('h.hour');
                $data = $node_record->value('data');
                //2016-10-12T12:31:51+1000
                $time = $year.'-'.$month.'-'.$day.'T'.$hour.':'.$data->value('time');
                $data = $data->values();
                unset($data['time']);
                $data['timeStamp'] = $time;
                array_push($all_data, $data);
            }
            $node_array = array('nodeID'=>$nodeID, 'data'=>$all_data);
            array_push($network_data, $node_array);
        }
        $network_array = array('networkID'=>$networkID, 'nodes'=>$network_data);
        array_push($overview, $network_array);
    }

    //var_dump($network_data);
    $_SESSION['data'] = array('success'=>true,'networks'=>$overview);
    return true;
}

function getNodeData($db, $input) {
    $count = count($input);
    if($count < 2) {
        $_SESSION['error'] = "Insufficient information to retrieve data!";
        return false;
    } else if(!isset($input['nodeID'])) {
        $_SESSION['error'] = "Node ID is missing!";
        return false;
    } else if(!isset($input['networkID'])) {
        $_SESSION['error'] = "Network ID is missing!";
        return false;
    } else if(!isset($input['skip'])) {
        $_SESSION['error'] = "Skip is missing!";
        return false;
    } else if(!isset($input['limit'])) {
        $_SESSION['error'] = "Limit is missing!";
        return false;
    } else if(!isset($input['dataType'])) {
        $_SESSION['error'] = "Data type is missing!";
        return false;
    }
    $networkID = $input['networkID'];
    $nodeID = $input['nodeID'];
    $dataType = $input['dataType'];
    $params = ['uuid' => $_SESSION['uuid'],'nodeID' => $nodeID, 'networkID' => $networkID, 'skip'=>intval($input['skip']), 'limit'=>intval($input['limit'])];
    try {
        $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) RETURN n", $params)->getRecords();
        if (count($result) != 1) {
            $_SESSION['error'] = "Network or node does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    try {
        $query = 'MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}})
            MATCH (n)-[:IN_YEAR]->(y:Year)-[:IN_MONTH]->(m:Month)-[:IN_DAY]->(d:Day)-[:IN_HOUR]->(h:Hour)-[:RECORDED]->(data:Data) WHERE ';
        foreach($dataType as $key=>$value) {
            $query = $query . "exists(data." .$value . ") OR ";
        }
        $query = substr($query, 0, -3).'RETURN y.year, m.month, d.day, h.hour, data, COLLECT([';
        foreach($dataType as $key=>$value) {
            $query = $query . "data." .$value . ",";
        }
        $query = substr($query, 0, -1)."]) AS maskData ORDER BY y.year+m.month+d.day+h.hour+data.time DESC SKIP {skip} LIMIT {limit}";
        $result = $db->run($query, $params)->getRecords();
        if (empty($result)) {
            $_SESSION['error'] = "Data does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $all_data = array();
    //var_dump($result);
    foreach ($result as $record) {
        $year = $record->value('y.year');
        $month = $record->value('m.month');
        $day = $record->value('d.day');
        $hour = $record->value('h.hour');
        $data = $record->value('data');
        $maskData = $record->value('maskData')[0];
        //2016-10-12T12:31:51+1000
        $time = $year.'-'.$month.'-'.$day.'T'.$hour.':'.$data->value('time');
        $newData = array();
        $newData['timeStamp'] = $time;
        foreach($dataType as $key=>$value) {
            $newData[$value] = $maskData[$key];
        }
        array_push($all_data, $newData);
    }
    $_SESSION['data'] = array('success'=> true, 'data'=>$all_data);
    return true;
}

function getNetworkData($db, $input) {
    $count = count($input);
    if($count < 2) {
        $_SESSION['error'] = "Insufficient information to sync data!";
        return false;
    } else if(!isset($input['networkID'])) {
        $_SESSION['error'] = "Network ID is missing!";
        return false;
    } else if(!isset($input['skip'])) {
        $_SESSION['error'] = "Skip is missing!";
        return false;
    } else if(!isset($input['limit'])) {
        $_SESSION['error'] = "Limit is missing!";
        return false;
    } else if(!isset($input['dataType'])) {
        $_SESSION['error'] = "Data type is missing!";
        return false;
    }
    $networkID = $input['networkID'];
    $dataType = $input['dataType'];
    $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID, 'skip'=>intval($input['skip']), 'limit'=>intval($input['limit'])];
    try {
        $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(n:Network{networkID:{networkID}}) RETURN n", $params)->getRecords();
        if (count($result) != 1) {
            $_SESSION['error'] = "Network  does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    try {
        $query = 'MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node)
            MATCH (n)-[:IN_YEAR]->(y:Year)-[:IN_MONTH]->(m:Month)-[:IN_DAY]->(d:Day)-[:IN_HOUR]->(h:Hour)-[:RECORDED]->(data:Data) WHERE ';
        foreach($dataType as $key=>$value) {
            $query = $query . "exists(data." .$value . ") OR ";
        }
        $query = substr($query, 0, -3).'RETURN y.year, m.month, d.day, h.hour, data, COLLECT([';
        foreach($dataType as $key=>$value) {
            $query = $query . "data." .$value . ",";
        }
        $query = substr($query, 0, -1)."]) AS maskData ORDER BY y.year+m.month+d.day+h.hour+data.time DESC SKIP {skip} LIMIT {limit}";
        $result = $db->run($query, $params)->getRecords();
        if (empty($result)) {
            $_SESSION['error'] = "Data does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $all_data = array();
    //var_dump($result);
    foreach ($result as $record) {
        $year = $record->value('y.year');
        $month = $record->value('m.month');
        $day = $record->value('d.day');
        $hour = $record->value('h.hour');
        $data = $record->value('data');
        $maskData = $record->value('maskData')[0];
        //2016-10-12T12:31:51+1000
        $time = $year.'-'.$month.'-'.$day.'T'.$hour.':'.$data->value('time');
        $newData = array();
        $newData['timeStamp'] = $time;
        foreach($dataType as $key=>$value) {
            $newData[$value] = $maskData[$key];
        }
        array_push($all_data, $newData);
    }
    $_SESSION['data'] = array('success'=> true, 'data'=>$all_data);
    return true;
}

function getUserData($db, $input) {
    $count = count($input);
    if($count < 2) {
        $_SESSION['error'] = "Insufficient information to sync data!";
        return false;
    }else if(!isset($input['skip'])) {
        $_SESSION['error'] = "Skip is missing!";
        return false;
    } else if(!isset($input['limit'])) {
        $_SESSION['error'] = "Limit is missing!";
        return false;
    } else if(!isset($input['dataType'])) {
        $_SESSION['error'] = "Data type is missing!";
        return false;
    }
    $dataType = $input['dataType'];
    $params = ['uuid' => $_SESSION['uuid'], 'skip'=>intval($input['skip']), 'limit'=>intval($input['limit'])];
    try {
        $result = $db->run("MATCH (n:User {uuid:{uuid}}) RETURN n", $params)->getRecords();
        if (count($result) != 1) {
            $_SESSION['error'] = "User does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    try {
        $query = 'MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network)-[:CONNECTED*]->(n:Node)
            MATCH (n)-[:IN_YEAR]->(y:Year)-[:IN_MONTH]->(m:Month)-[:IN_DAY]->(d:Day)-[:IN_HOUR]->(h:Hour)-[:RECORDED]->(data:Data) WHERE ';
        foreach($dataType as $key=>$value) {
            $query = $query . "exists(data." .$value . ") OR ";
        }
        $query = substr($query, 0, -3).'RETURN y.year, m.month, d.day, h.hour, data, COLLECT([';
        foreach($dataType as $key=>$value) {
            $query = $query . "data." .$value . ",";
        }
        $query = substr($query, 0, -1)."]) AS maskData ORDER BY y.year+m.month+d.day+h.hour+data.time DESC SKIP {skip} LIMIT {limit}";
        $result = $db->run($query, $params)->getRecords();
        if (empty($result)) {
            $_SESSION['error'] = "Data does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $all_data = array();
    //var_dump($result);
    foreach ($result as $record) {
        $year = $record->value('y.year');
        $month = $record->value('m.month');
        $day = $record->value('d.day');
        $hour = $record->value('h.hour');
        $data = $record->value('data');
        $maskData = $record->value('maskData')[0];
        //2016-10-12T12:31:51+1000
        $time = $year.'-'.$month.'-'.$day.'T'.$hour.':'.$data->value('time');
        $newData = array();
        $newData['timeStamp'] = $time;
        foreach($dataType as $key=>$value) {
            $newData[$value] = $maskData[$key];
        }
        array_push($all_data, $newData);
    }
    $_SESSION['data'] = array('success'=> true, 'data'=>$all_data);
    return true;
}

function getNodeDataT($db, $input) {
    $count = count($input);
    if($count < 2) {
        $_SESSION['error'] = "Insufficient information to sync data!";
        return false;
    } else if(!isset($input['nodeID'])) {
        $_SESSION['error'] = "Node ID is missing!";
        return false;
    } else if(!isset($input['networkID'])) {
        $_SESSION['error'] = "Network ID is missing!";
        return false;
    } else if(!isset($input['timeStamp'])) {
        $_SESSION['error'] = "Skip is missing!";
        return false;
    } else if(!isset($input['dataType'])) {
        $_SESSION['error'] = "Data type is missing!";
        return false;
    }
    $networkID = $input['networkID'];
    $nodeID = $input['nodeID'];
    $timeStamp = $input['timeStamp'];
    $dataType = $input['dataType'];
    $params = ['uuid' => $_SESSION['uuid'],'nodeID' => $nodeID, 'networkID' => $networkID];
    try {
        $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) RETURN n", $params)->getRecords();
        if (count($result) != 1) {
            $_SESSION['error'] = "Network or node does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    try {
        $query = 'MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}})
            MATCH (n)-[:IN_YEAR]->(y:Year)-[:IN_MONTH]->(m:Month)-[:IN_DAY]->(d:Day)-[:IN_HOUR]->(h:Hour)-[:RECORDED]->(data:Data) WHERE (';
        foreach($dataType as $key=>$value) {
            $query = $query . "exists(data." .$value . ") OR ";
        }
        $query = substr($query, 0, -3);
        $query = $query.') AND y.year+"-"+m.month+"-"+d.day+"T"+h.hour+":"+data.time >= "'.$timeStamp.'" RETURN y.year, m.month, d.day, h.hour, data, COLLECT([';
        foreach($dataType as $key=>$value) {
            $query = $query . "data." .$value . ",";
        }
        $query = substr($query, 0, -1)."]) AS maskData ORDER BY y.year+m.month+d.day+h.hour+data.time DESC";
        //echo $query;
        $result = $db->run($query, $params)->getRecords();
        if (empty($result)) {
            $_SESSION['error'] = "Data does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $all_data = array();
    //var_dump($result);
    foreach ($result as $record) {
        $year = $record->value('y.year');
        $month = $record->value('m.month');
        $day = $record->value('d.day');
        $hour = $record->value('h.hour');
        $data = $record->value('data');
        $maskData = $record->value('maskData')[0];
        //2016-10-12T12:31:51+1000
        $time = $year.'-'.$month.'-'.$day.'T'.$hour.':'.$data->value('time');
        $newData = array();
        $newData['timeStamp'] = $time;
        foreach($dataType as $key=>$value) {
            $newData[$value] = $maskData[$key];
        }
        array_push($all_data, $newData);
    }
    $_SESSION['data'] = array('success'=> true, 'data'=>$all_data);
    return true;
}

function getNetworkDataT($db, $input) {
    $count = count($input);
    if($count < 2) {
        $_SESSION['error'] = "Insufficient information to sync data!";
        return false;
    } else if(!isset($input['networkID'])) {
        $_SESSION['error'] = "Network ID is missing!";
        return false;
    } else if(!isset($input['timeStamp'])) {
        $_SESSION['error'] = "Skip is missing!";
        return false;
    } else if(!isset($input['dataType'])) {
        $_SESSION['error'] = "Data type is missing!";
        return false;
    }
    $networkID = $input['networkID'];
    $timeStamp = $input['timeStamp'];
    $dataType = $input['dataType'];
    $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID];
    try {
        $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(n:Network{networkID:{networkID}}) RETURN n", $params)->getRecords();
        if (count($result) != 1) {
            $_SESSION['error'] = "Network does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    try {
        $query = 'MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node)
            MATCH (n)-[:IN_YEAR]->(y:Year)-[:IN_MONTH]->(m:Month)-[:IN_DAY]->(d:Day)-[:IN_HOUR]->(h:Hour)-[:RECORDED]->(data:Data) WHERE (';
        foreach($dataType as $key=>$value) {
            $query = $query . "exists(data." .$value . ") OR ";
        }
        $query = substr($query, 0, -3);
        $query = $query.') AND y.year+"-"+m.month+"-"+d.day+"T"+h.hour+":"+data.time >= "'.$timeStamp.'" RETURN y.year, m.month, d.day, h.hour, data, COLLECT([';
        foreach($dataType as $key=>$value) {
            $query = $query . "data." .$value . ",";
        }
        $query = substr($query, 0, -1)."]) AS maskData ORDER BY y.year+m.month+d.day+h.hour+data.time DESC";
        //echo $query;
        $result = $db->run($query, $params)->getRecords();
        if (empty($result)) {
            $_SESSION['error'] = "Data does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $all_data = array();
    //var_dump($result);
    foreach ($result as $record) {
        $year = $record->value('y.year');
        $month = $record->value('m.month');
        $day = $record->value('d.day');
        $hour = $record->value('h.hour');
        $data = $record->value('data');
        $maskData = $record->value('maskData')[0];
        //2016-10-12T12:31:51+1000
        $time = $year.'-'.$month.'-'.$day.'T'.$hour.':'.$data->value('time');
        $newData = array();
        $newData['timeStamp'] = $time;
        foreach($dataType as $key=>$value) {
            $newData[$value] = $maskData[$key];
        }
        array_push($all_data, $newData);
    }
    $_SESSION['data'] = array('success'=> true, 'data'=>$all_data);
    return true;
}

function getUserDataT($db, $input) {
    $count = count($input);
    if($count < 2) {
        $_SESSION['error'] = "Insufficient information to sync data!";
        return false;
    } else if(!isset($input['timeStamp'])) {
        $_SESSION['error'] = "Skip is missing!";
        return false;
    } else if(!isset($input['dataType'])) {
        $_SESSION['error'] = "Data type is missing!";
        return false;
    }
    $timeStamp = $input['timeStamp'];
    $dataType = $input['dataType'];
    $params = ['uuid' => $_SESSION['uuid']];
    try {
        $result = $db->run("MATCH (n:User {uuid:{uuid}}) RETURN n", $params)->getRecords();
        if (count($result) != 1) {
            $_SESSION['error'] = "User does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    try {
        $query = 'MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network)-[:CONNECTED*]->(n:Node)
            MATCH (n)-[:IN_YEAR]->(y:Year)-[:IN_MONTH]->(m:Month)-[:IN_DAY]->(d:Day)-[:IN_HOUR]->(h:Hour)-[:RECORDED]->(data:Data) WHERE (';
        foreach($dataType as $key=>$value) {
            $query = $query . "exists(data." .$value . ") OR ";
        }
        $query = substr($query, 0, -3);
        $query = $query.') AND y.year+"-"+m.month+"-"+d.day+"T"+h.hour+":"+data.time >= "'.$timeStamp.'" RETURN y.year, m.month, d.day, h.hour, data, COLLECT([';
        foreach($dataType as $key=>$value) {
            $query = $query . "data." .$value . ",";
        }
        $query = substr($query, 0, -1)."]) AS maskData ORDER BY y.year+m.month+d.day+h.hour+data.time DESC";
        //echo $query;
        $result = $db->run($query, $params)->getRecords();
        if (empty($result)) {
            $_SESSION['error'] = "Data does not exist!";
            return false;
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $all_data = array();
    //var_dump($result);
    foreach ($result as $record) {
        $year = $record->value('y.year');
        $month = $record->value('m.month');
        $day = $record->value('d.day');
        $hour = $record->value('h.hour');
        $data = $record->value('data');
        $maskData = $record->value('maskData')[0];
        //2016-10-12T12:31:51+1000
        $time = $year.'-'.$month.'-'.$day.'T'.$hour.':'.$data->value('time');
        $newData = array();
        $newData['timeStamp'] = $time;
        foreach($dataType as $key=>$value) {
            $newData[$value] = $maskData[$key];
        }
        array_push($all_data, $newData);
    }
    $_SESSION['data'] = array('success'=> true, 'data'=>$all_data);
    return true;
}

function getNodeTypes($db, $input) {
    $networkID = $input['networkID'];
    $nodeID = $input['nodeID'];
    $params = ['uuid' => $_SESSION['uuid'],'nodeID' => $nodeID, 'networkID' => $networkID];
    try {
        $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) RETURN n.dataType", $params)->getRecords();
        if (count($result) != 1) {
            $_SESSION['error'] = "Network or node does not exist!";
            return false;
        } else {
            $dataType = $result[0]->values()[0];
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }
    $_SESSION['data'] = array('success'=> true, 'dataType'=>$dataType);
    return true;
}