<?php
function registerTrigger($db, $input) {
    $networkID = $input['networkID'];
    $nodeID = $input['nodeID'];
    $dataType = $input['dataType'];
    $compare = $input['compare'];
    $value = $input['value'];
    $script = $input['script'];
    try {
        $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID, 'nodeID' => $nodeID, 'dataType' => $dataType,
            'compare' => $compare, 'value' => $value, 'script' => $script];
        $node = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}}) RETURN n", $params)->getRecords();
        $trigger = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(Network{networkID:{networkID}})-[:CONNECTED*]->(:Node{nodeID:{nodeID}})-[:ATTACHED]->(n:Trigger{dataType:{dataType}}) RETURN n", $params)->getRecords();
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        return false;
    }

    if (empty($node)) {
        $_SESSION['error'] = "Network or node does not exist.";
        return false;
    }

    if (!empty($trigger)) {
        $_SESSION['error'] = "Node ID already exists on this dataType for this node.";
        return false;
    }else {
        try {
            $result = $db->run("MATCH (User {uuid:{uuid}})-[:OWNS]->(ne:Network{networkID:{networkID}})-[:CONNECTED*]->(n:Node{nodeID:{nodeID}})
                CREATE (t:Trigger {dataType:{dataType}, compare:{compare}, value:{value}, script:{script}}) CREATE (n)-[:ATTACHED]->(t)", $params);
        } catch (DBException $e) {
            $_SESSION['error'] = "Query Failed!";
            return false;
        }
        return true;
    }
}

function viewTriggers($db, $input) {
    $networkID = $input['networkID'];
    $nodeID = $input['nodeID'];
    try {
        $params = ['uuid' => $_SESSION['uuid'], 'networkID' => $networkID, 'nodeID' => $nodeID];
        $result = $db->run("MATCH (:User {uuid:{uuid}})-[:OWNS]->(:Network {networkID:{networkID}})-[:CONNECTED*]->(:Node{nodeID:{nodeID}})-[:ATTACHED]->(n:Trigger) RETURN n", $params)->getRecords();
        $triggers = [];
        foreach ($result as $record) {
            $triggers[] = $record->get('n')->values();
        }
    } catch (DBException $e) {
        $_SESSION['error'] = "Query Failed!";
        $output = array('success' => false);
        return $output;
    }
    if (empty($triggers)){
        $_SESSION['error'] = "No triggers are found!";
        $output = array('success' => false);
    } else {
        $count = count($triggers);
        $output = array('success' => true, 'count' => $count, 'triggers' => $triggers);
    }
    return $output;
}