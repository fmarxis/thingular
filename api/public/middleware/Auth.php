<?php

class Auth
{
    private $db;
    private $logger;
    private $whiteList;

    public function __construct($container) {
        //Define the urls that you want to exclude from Authentication, aka public urls
        $this->whiteList = array('user\/signin', 'user\/signup', 'utils', 'user\/logout', 'user\/checkToken', 'sync');
        $this->db = $container->db;
        $this->logger = $container->logger;
    }

    public function __invoke($request,$response, $next)
    {
        if($this->isPublicUrl($request->getUri()->getPath())){
            $origin = "*";
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                $origin = $_SERVER["HTTP_ORIGIN"];
            }
            $response = $next($request, $response);
        } else {
            $input = $request->getParsedBody();
            if(isset($_COOKIE['uuid']) && isset($_COOKIE['token'])) {
                if(verifyToken($this->db, $_COOKIE['uuid'], $_COOKIE['token'])) {
                    unset($input['UUID']);
                    unset($input['token']);
                    $_SESSION['uuid'] = $_COOKIE['uuid'];
                    $_SESSION['token'] = $_COOKIE['token'];
                    $newRequest = $request->withParsedBody($input);
                    $response = $next($newRequest, $response);
                } else {
                    logoutUser();
                    $_SESSION['error'] = "Invalid cookies, please authenticate again";
                    $response = $this->deny_access($response)->withStatus(401)->withAddedHeader("Content-Type", "application/json;charset=utf-8");
                }
            } else if(isset($_SESSION['uuid']) && isset($_SESSION['token'])) {
                if(verifyToken($this->db, $_SESSION['uuid'], $_SESSION['token'])) {
                    unset($input['UUID']);
                    unset($input['token']);
                    $newRequest = $request->withParsedBody($input);
                    $response = $next($newRequest, $response);
                } else {
                    logoutUser();
                    $_SESSION['error'] = "Invalid session, please authenticate again";
                    $response = $this->deny_access($response)->withStatus(401)->withAddedHeader("Content-Type", "application/json;charset=utf-8");
                }
            } else if(isset($input['UUID']) && isset($input['token'])) {
                if(verifyToken($this->db, $input['UUID'], $input['token'])) {
                    unset($input['UUID']);
                    unset($input['token']);
                    $_SESSION['uuid'] = $input['uuid'];
                    $_SESSION['token'] = $input['token'];
                    $newRequest = $request->withParsedBody($input);
                    $response = $next($newRequest, $response);
                } else {
                    logoutUser();
                    $_SESSION['error'] = "Invalid token, please authenticate again";
                    $response = $this->deny_access($response)->withStatus(401)->withAddedHeader("Content-Type", "application/json;charset=utf-8");
                }
            } else {
                logoutUser();
                $_SESSION['error'] = "Lacking authentication details, please authenticate";
                $response = $this->deny_access($response)->withStatus(401)->withAddedHeader("Content-Type", "application/json;charset=utf-8");
            }
        }
        $origin = "*";
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            $origin = $_SERVER["HTTP_ORIGIN"];
        }
        if ($request->getMethod() == "OPTIONS") {
            $response = $this->deny_access($response)->withStatus(200)->withAddedHeader("Content-Type", "application/json;charset=utf-8");
        }
        return $response->withAddedHeader("Access-Control-Allow-Origin", $origin)->withAddedHeader("Access-Control-Allow-Headers", "Content-Type, Accept")
            ->withAddedHeader("Access-Control-Allow-Credentials", "true");
    }

    public function deny_access($response) {
        $output = array('success' => false, 'reason' => $_SESSION['error']);
        $response->withJson($output);
        return $response;
    }

    public function isPublicUrl($url) {
        $patterns_flattened = implode('|', $this->whiteList);
        $matches = null;
        preg_match('/' . $patterns_flattened . '/', $url, $matches);
        return (count($matches) > 0);
    }
}
?>