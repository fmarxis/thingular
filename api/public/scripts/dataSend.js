var socket = require('socket.io-client')('https://t.th1nk.io:8866');
var data = {
  	"nodeID": "1",
    "timeStamp": "2016-10-12T13:31:40+1000",
    "speed": "20",
    "temperature": "10",
    "lon": "153.0163",
    "lat": "-27.4995",
};

var lon = 153.0163;
var lon_lower = 153.0128;
var lon_upper = 153.0207;
var lon_step = 0.0003;

var lat = -27.4995;
var lat_lower = -27.5017;
var lat_upper = -27.4977;
var lat_step = 0.0003;


//For testing purposes
function dataGenerator() {
	var d = new Date();
	var n = d.toISOString().substr(0,19)+"+1000";
	data.timeStamp = n;
	var value = 0;
	value = Math.random()*100+20;
	data.speed = value.toString();
	value = Math.random()*5+25;
	data.temperature = value.toString();
	if (Math.random() > 0.5) {
		if ((lon + lon_step) < lon_upper) {
			lon = lon + lon_step;
		}
	} else {
		if ((lon - lon_step) > lon_lower) {
			lon = lon - lon_step;
		}
	}
	if (Math.random() > 0.5) {
		if ((lat + lat_step) < lat_upper) {
			lat = lat + lat_step;
		}
	} else {
		if ((lat - lat_step) > lat_lower) {
			lat = lat - lat_step;
		}
	}
	data.lon = lon.toString();
	data.lat = lat.toString();
	data["pubKey"] = "e2bea95ed226bed0fd2eb17829cb8980fc686fea";
	socket.emit('sync', JSON.stringify(data));
	delete data["pubKey"];
	console.log("Sent");
}

socket.on('connect', function(){
    
});
socket.on('chat message', function(data){

});
socket.on('disconnect', function(){

});

setInterval(dataGenerator, 60000);