## compute_input.py

import sys, json, numpy as np

#Read data from stdin
def read_in():
    line = sys.stdin.readline()
    print("Your input was: " + line)
    #Since our input would only be having one line, parse our JSON data from that
    return json.loads(line)

def main():
    print("Python Script Started")
    print('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))
    print("This program sums an input json array of numbers:")
    #get our data as an array from read_in()
    lines = read_in()

    #create a numpy array
    np_lines = np.array(lines)

    #use numpys sum method to find sum of all elements in the array
    lines_sum = np.sum(np_lines)

    #return the sum to the output stream
    print("Output is: ")
    print(lines_sum)
    print("Python Script Exiting")
    sys.stdout.flush()
    sys.stderr.flush()

#start process
if __name__ == '__main__':
    main()