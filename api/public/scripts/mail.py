# -*- coding: utf-8 -*-

 #########################
##       Imports         ##
 #########################

import sys
import re
import subprocess

 #########################
##   Private Functions   ##
 #########################

def handle_arguments (arguments):

    if len(arguments) < 4:
        print "Wrong number of Arguments"
        print "Usage: <scriptName.py> <destination email address> <subject> <content>"
        return None
    
    argumentsDictionary = {}

    try:
        arguments.pop(0)
    except:
        print "Argument error"
        print "Usage: <scriptName.py> <destination email address> <subject> <content>"
        return None

    emailRegex = re.compile(r"[^@]+@[^@]+\.[^@]+")
    if not emailRegex.match(arguments[0]):
        print "Invalid email address"
        return None

    argumentsDictionary['destinationAddress'] = arguments[0]

    argumentsDictionary['subject'] = arguments[1]
    argumentsDictionary['content'] = arguments[2]

    return argumentsDictionary

 #########################
##         MAIN          ##
 #########################

def main (destinationAddress, subject, content, sender):
    
    bashCommand = "echo {} | mail -s {} -r {} {}".format(content, subject, sender, destinationAddress)

    process = subprocess.Popen(bashCommand, shell=True)

 #########################
## Run from Command Line ##
 #########################

if __name__ == '__main__':
    
    # Arguments from Command Line
    # 
    # sys.argv = [pythonMAILXscript.py, arg1, agr2, ...]

    argumentsDictionary = handle_arguments(sys.argv)

    if argumentsDictionary is not None:
        
        destinationAddress = argumentsDictionary['destinationAddress']
        subject = argumentsDictionary['subject'].replace(" ", "\ ")
        content = argumentsDictionary['content']
        sender = "Thingular\ Alert\<alert@th1nk.io\>"

        main(destinationAddress, subject, content, sender)
