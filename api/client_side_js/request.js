var passPhrase = "!s12(9UK^FsK*@";

function postRequest() {
    var request = document.getElementById("content").value;
    var encrypted = CryptoJS.AES.encrypt(document.getElementById("content").value, passPhrase, {format: CryptoJSAesJson}).toString();
    console.log(document.getElementById("content").value);
    console.log(encrypted);
    $.ajax({
        type: document.getElementById("method").value,
        url: document.getElementById("url").value,
        data: encrypted,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
            console.log("Post Success:" + JSON.stringify(data));
        },
        error: function(errMsg){
            console.log("Post Failure:" + JSON.stringify(errMsg));
        },
        complete: function(){
            console.log("Complete");
        }
    });
}

function encryptData() {
    var en = CryptoJS.AES.encrypt(document.getElementById("input").value, passPhrase, {format: CryptoJSAesJson}).toString();
    document.getElementById("encrypt").value = en;
}

function decryptData() {
    var de = CryptoJS.AES.decrypt(document.getElementById("input2").value, passPhrase, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8);
    document.getElementById("decrypt").value = de;
}