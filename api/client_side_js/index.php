<?php
?>
AES Crypto
<script src="resources/libraries/jquery-1.11.3.min.js"></script>
<script src="resources/libraries/aes.js"></script>
<script src="resources/js/crypto.js"></script>
<script src="resources/js/request.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css">
<form action="#" onsubmit="encryptData(); return false;" method="post">
	Data
	<textarea rows="10" cols="70" id="input"></textarea>
	<input type="submit" value="Encrypt">
	Encrypted
	<textarea rows="10" cols="70" id="encrypt"></textarea>
</form>
<form action="#" onsubmit="decryptData(); return false;" method="post">
	Encrypted
	<textarea rows="10" cols="70" id="input2"></textarea>
	<input type="submit" value="Decrypt">
	Data
	<textarea rows="10" cols="70" id="decrypt"></textarea>
</form>
