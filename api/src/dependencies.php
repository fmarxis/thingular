<?php
// DIC configuration

use GraphAware\Neo4j\Client\ClientBuilder;

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// Neo4j Connection
$container['db'] = function ($c) {
    $db = $c['settings']['config']['db'];
    $client = ClientBuilder::create()
    ->addConnection('bolt', 'bolt://'.$db['user'].':'.$db['pass'].'@'.$db['host'].':7687')
    ->addConnection('http', 'http://'.$db['user'].':'.$db['pass'].'@'.$db['host'].':7474')
    ->build();
    return $client;
};

$container['errorHandler'] = function ($c) {
  return function ($request, $response, $exception) use ($c) {
    $data = [
      'code' => $exception->getCode(),
      'message' => $exception->getMessage(),
      'file' => $exception->getFile(),
      'line' => $exception->getLine(),
      'trace' => explode("\n", $exception->getTraceAsString()),
    ];

    return $c->get('response')->withStatus(500)
             ->withHeader('Content-Type', 'application/json')
             ->write(json_encode($data));
  };
};