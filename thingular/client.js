var socket = require('socket.io-client')('https://localhost:8866');
socket.on('connect', function(){
    socket.emit('room', process.argv[2]);
});
socket.on('update', function(msg){
    console.log(msg);
});
socket.on('chat message', function(msg){
    console.log(msg);
});
socket.on('disconnect', function(){

});