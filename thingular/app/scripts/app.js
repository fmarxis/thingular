/**
 * Thingular Angular App
 *
 */
(function () {
    angular.module('thingular', [
        'ui.router',                // Angular flexible routing
        'ui.bootstrap',             // AngularJS native directives for Bootstrap
        'angular-flot',             // Flot chart
        'datamaps',                 // Datamaps directive
        'ngAnimate',                // Angular animations
        'toastr',                   // Toastr notification
        'ui.sortable',              // AngularJS ui-sortable
        'datatables',               // Angular datatables plugin
        'datatables.buttons',       // Datatables Buttons
        'ui.tree',                  // Angular ui Tree
        'bootstrap.angular.validation', //Angular Bootstrap Validation
        'angular-ladda',            // Angular Ladda Button
        'ngScrollbars',             // Angular Scrollbar
        'openlayers-directive',     // Openlayers directive
    ])
})();

