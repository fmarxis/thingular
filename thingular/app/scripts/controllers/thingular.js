// Thingular Main Controller

angular
    .module('thingular')
    .controller('thingularCtrl', thingularCtrl);

angular
    .module('thingular')
    .service('Data', function () {
        var feed = new Array();
        var user = [];
        var dataLoaded = false;
        var updateTime = "12:34AM";
        var neT = 0;
        var noT = 0;
        var trT = 0;
        var liT = 0;
        var dataT = [];
        var dataSel = {};
        var selNodeID = -1;
        var selNetworkID = -1;
        var networkSel = String(-1);
        var nodeSel = String(-1);
        var Node1 = {
            "name": "Libelium Node 1",
            "lat": -27.4997,
            "lon": 153.0158,
            "label": {
                "message": "",
                "show": false,
                "showOnMouseOver": true
            }
        };
        var Node2 = {
            "name": "Libelium Node 2",
            "lat": -27.4987,
            "lon": 153.0165,
            "label": {
                "message": "",
                "show": false,
                "showOnMouseOver": true
            }
        };
        var Node3 = {
            "name": "Libelium Node 3",
            "lat": -27.4979,
            "lon": 153.0173,
            "label": {
                "message": "",
                "show": false,
                "showOnMouseOver": true
            }
        };
        var Node4 = {
            "name": "Libelium Node 4",
            "lat": -27.4993,
            "lon": 153.0171,
            "label": {
                "message": "",
                "show": false,
                "showOnMouseOver": true
            }
        };
        var NodeTest = {
            "name": "Test Node in Network:Home Node:N1",
            "lat": -27.4995,
            "lon": 153.0163,
            "label": {
                "message": "",
                "show": false,
                "showOnMouseOver": true
            }
        };
        return {
            getFeed: function () {
                return feed;
            },
            getUser: function () {
                return user;
            },
            getDataLoaded: function () {
                return dataLoaded;
            },
            getUpdateTime: function () {
                return updateTime;
            },
            getNeT: function () {
                return neT;
            },
            getNoT: function () {
                return noT;
            },
            getTrT: function () {
                return trT;
            },
            getLiT: function () {
                return liT;
            },
            getDataT: function () {
                return dataT;
            },
            getSelNodeID: function () {
                return selNodeID;
            },
            getSelNetworkID: function () {
                return selNetworkID;
            },
            getDataSel: function () {
                return dataSel;
            },
            getNetworkSel: function () {
                return networkSel;
            },
            getNodeSel: function () {
                return nodeSel;
            },
            setFeed: function (n) {
                feed = n;
            },
            setUser: function (n) {
                user = n;
            },
            setDataLoaded: function (n) {
                dataLoaded = n;
            },
            setUpdateTime: function (n) {
                updateTime = n;
            },
            setNeT: function (n) {
                neT = n;
            },
            setNoT: function (n) {
                noT = n;
            },
            setTrT: function (n) {
                trT = n;
            },
            setLiT: function (n) {
                liT = n;
            },
            setDataT: function (n) {
                dataT = n;
            },
            setSelNodeID: function (n) {
                selNodeID = n;
            },
            setSelNetworkID: function (n) {
                selNetworkID = n;
            },
            setDataSel: function (n) {
                dataSel = n;
            },
            setNetworkSel: function (n) {
                networkSel = n;
            },
            setNodeSel: function (n) {
                nodeSel = n;
            },
            getNode1: function () {
                return Node1;
            },
            setNode1: function (n) {
                Node1 = n;
            },
            getNode2: function () {
                return Node2;
            },
            setNode2: function (n) {
                Node2 = n;
            },
            getNode3: function () {
                return Node3;
            },
            setNode3: function (n) {
                Node3 = n;
            },
            getNode4: function () {
                return Node4;
            },
            setNode4: function (n) {
                Node4 = n;
            },
            getNodeTest: function () {
                return NodeTest;
            },
            setNodeTest: function (n) {
                NodeTest = n;
            },
        };
    });

angular.module('thingular')
    .config(['bsValidationConfigProvider', function(bsValidationConfigProvider) {
        bsValidationConfigProvider.global.errorClass = 'has-warning';
        bsValidationConfigProvider.global.addSuccessClass = false;
        bsValidationConfigProvider.global.errorMessagePrefix = '<span class="glyphicon glyphicon-warning-sign"></span> &nbsp;';
        bsValidationConfigProvider.global.setValidateFieldsOn('submit');
    }]);

angular.module('thingular')
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.withCredentials = true;
    }]);

function thingularCtrl($scope, $state, $http, toastr, toastrConfig, userInfo, Data, $interval){
    angular.extend(toastrConfig, {
        newestOnTop: true,
        positionClass: 'toast-bottom-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        progressBar: true
    });
    $scope.s_config = {
        autoHideScrollbar: false,
        theme: 'light-3',
        advanced:{
            updateOnContentResize: true
        },
        setHeight: 120,
        scrollInertia: 0
    };
    $scope.c_config = {
        autoHideScrollbar: false,
        theme: 'light-3',
        advanced:{
            updateOnContentResize: true
        },
        setHeight: 400,
        scrollInertia: 0
    };

    $scope.reset_info = "Your address email to send a new temporary password";

    $scope.dataLoaded = Data.getDataLoaded();
    $scope.user = Data.getUser();
    $scope.feed = Data.getFeed();
    $scope.updateTime = Data.getUpdateTime();
    $scope.neT = Data.getNeT();
    $scope.noT = Data.getNoT();
    $scope.trT = Data.getTrT();
    $scope.liT = Data.getLiT();
    $scope.dataT = Data.getDataT();
    $scope.dataSel = Data.getDataSel();
    $scope.selNodeID = Data.getSelNodeID();
    $scope.selNetworkID = Data.getSelNetworkID();
    $scope.networkSel = Data.getNetworkSel();
    $scope.nodeSel = Data.getNodeSel();
    $scope.Node1 = Data.getNode1();
    $scope.Node2 = Data.getNode2();
    $scope.Node3 = Data.getNode3();
    $scope.Node4 = Data.getNode4();
    $scope.NodeTest = Data.getNodeTest();
    $scope.rnID = "";
    $scope.rnName  = "";
    $scope.rnDes  = "";
    $scope.rnoNetwork = "";
    $scope.rnoID = "";
    $scope.rnoName  = "";
    $scope.rnoConnect  = "";
    $scope.rnoLat  = "";
    $scope.rnoLon  = "";
    $scope.rnoDes  = "";
    $scope.rtrNetwork  = 0;
    $scope.rtrNode  = 0;
    $scope.rtrType  = "";
    $scope.rtrComp  = "";
    $scope.rtrValue  = "";
    $scope.rtrScript  = "";
    $scope.rliNetwork = 0;
    $scope.rliNode = 0;
    $scope.rliScript = "";


    $scope.updateTypeSel = function(mode) {
        var ref = $scope.user;
        if (mode == 1) {
            if ($scope.networkSel == "-1") {
                $scope.selNetworkID = -1;
            } else {
                ref = ref.networks[$scope.networkSel];
                $scope.selNetworkID = ref.networkID;
            }
            Data.setSelNetworkID($scope.selNetworkID);
            $scope.nodeSel = String(-1);
            Data.setNodeSel($scope.nodeSel);
            $scope.selNodeID = -1;
            Data.setSelNodeID($scope.selNodeID);
        } else if (mode == 2) {
            if ($scope.nodeSel == "-1") {
                $scope.selNodeID = -1;
            } else {
                ref = ref.networks[$scope.networkSel].nodes[$scope.nodeSel];
                $scope.selNodeID = ref.nodeID;
            }
            Data.setSelNodeID($scope.selNodeID);
        }
        $scope.dataT = ref.dataType;
        Data.setDataT($scope.dataT);
        $scope.dataSel = {};
    };

    $scope.process401 = function (response) {
        if(response.status == -1) {
            $scope.popError("401 Unauthorized");
            $state.go('test.login');
        } else {
            if("success" in response.data) {
                if (response.data["success"] == false) {
                    $scope.popWarning(response.data["reason"]);
                }
            } else {
                $scope.popError("401 Unauthorized");
            }
            $state.go('test.login');
        }
        waitFor = false;
    };

    $scope.popInfo = function (msg) {
        if (msg == null) {
            msg = "Thingular";
        }
        toastr.info(msg, {});
    };

    $scope.popSuccess = function (msg) {
        if (msg == null) {
            msg = "Thingular";
        }
        toastr.success('Success - ' + msg, {});
    };

    $scope.popWarning = function (msg) {
        if (msg == null) {
            msg = "Thingular";
        }
        toastr.warning('Warning - ' + msg, {});
    };

    $scope.popError = function (msg) {
        if (msg == null) {
            msg = "Thingular";
        }
        toastr.error('Error - ' + msg, {});
    };

    $scope.login = function () {
        $scope.submit = true;
        if ($scope.remember == null) {
            $scope.remember = false;
        }
        $http({
            method : "POST",
            url : host + "/user/signin",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "user": {
                    "email": $scope.email,
                    "password": $scope.password,
                    "remember": $scope.remember
                }
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    $scope.popSuccess("Authentication was successful!");
                    $scope.authCheck();
                } else {
                    $scope.popWarning(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.signup = function () {
        $scope.submit = true;
        $http({
            method : "POST",
            url : host + "/user/signup",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "user": {
                    "email": $scope.email,
                    "password": $scope.password,
                    "firstName": $scope.firstName,
                    "lastName": $scope.lastName
                }
            }
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    $scope.popSuccess("Your account was successfully registered!");
                    $state.go('test.login');
                } else {
                    $scope.popWarning(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.reset = function () {
        $scope.submit = true;
        $http({
            method : "GET",
            url : host + "/user/authState",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            withCredentials: true
        }).then(function success(response) {
            $scope.popSuccess(response.data);
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.authCheck = function () {
        $http({
            method : "GET",
            url : host + "/user/authState",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    $scope.popSuccess("Log in Successful!");
                    loginStatus = true;
                    $scope.user = response.data["user"];
                    Data.setUser($scope.user);
                    userInfo.setUserInfo($scope.user);
                    $state.go('thingular.dashboard');
                } else {
                    loginStatus = false;
                    $scope.popWarning(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
        }, function error(response) {
            loginStatus = false;
        });
    };

    $scope.logout = function () {
        $http({
            method : "GET",
            url : host + "/user/logout",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    $scope.popSuccess("Log Out Successful!");
                    loginStatus = false;
                    $scope.user = [];
                    Data.setUser($scope.user);
                    $scope.dataLoaded = false;
                    $state.go('test.login');
                } else {
                    $scope.popWarning(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.viewNetworks = function () {
        $http({
            method : "POST",
            url : host + "/view/network",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {

            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    networkCount = response.data["count"];
                    $scope.neT = networkCount;
                    Data.setNeT($scope.neT);
                    networks = response.data["networks"];
                    $scope.user.networkCount = networkCount;
                    $scope.user.networks = networks;
                } else {
                    $scope.user.networkCount = 0;
                    $scope.user.networks = [];
                }
                Data.setUser($scope.user);
                var arrayLength = $scope.user.networks.length;
                for (var i = 0; i < arrayLength; i++) {
                    var network = $scope.user.networks[i];
                    $scope.user.dataType = [];
                    $scope.user.networks[i].dataType = [];
                    setTimeout($scope.viewNodes(network['networkID'],i), 2000);
                }
                Data.setUser($scope.user);
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.viewNodes = function (networkID, num) {
        $http({
            method : "POST",
            url : host + "/view/node",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": networkID,
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    nodeCount = response.data["count"];
                    nodes = response.data["nodes"];
                    $scope.user.networks[num].nodeCount = nodeCount;
                    $scope.user.networks[num].nodes = nodes;
                    $scope.noT += nodeCount;
                    Data.setNoT($scope.noT);
                } else {
                    $scope.user.networks[num].nodeCount = 0;
                    $scope.user.networks[num].nodes = [];
                }
                Data.setUser($scope.user);
                var arrayLength = $scope.user.networks[num].nodes.length;
                for (var i = 0; i < arrayLength; i++) {
                    var node = $scope.user.networks[num].nodes[i];
                    $scope.viewTriggers(networkID,node.nodeID, num, i);
                    $scope.viewListeners(networkID,node.nodeID, num, i);
                    for (var j=0; j < node['dataType'].length; j++) {
                        if ($scope.user.dataType.includes(node['dataType'][j]) == false) {
                            $scope.user.dataType.push(node['dataType'][j]);
                        }
                        if ($scope.user.networks[num].dataType.includes(node['dataType'][j]) == false) {
                            $scope.user.networks[num].dataType.push(node['dataType'][j]);
                        }
                    }
                }
                Data.setUser($scope.user);
                $scope.updateTypeSel();
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.viewTriggers = function (networkID, nodeID, netlistPos, nodelistPos) {
        $http({
            method : "POST",
            url : host + "/view/trigger",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": networkID,
                "nodeID": nodeID,
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    triggerCount = response.data["count"];
                    triggers = response.data["triggers"];
                    $scope.trT += triggerCount;
                    Data.setTrT($scope.trT);
                } else {
                    triggerCount = 0;
                    triggers = [];
                }
                $scope.user.networks[netlistPos].nodes[nodelistPos].triggerCount = triggerCount;
                $scope.user.networks[netlistPos].nodes[nodelistPos].triggers = triggers;
                Data.setUser($scope.user);
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.viewListeners = function (networkID, nodeID, netlistPos, nodelistPos) {
        $http({
            method : "POST",
            url : host + "/view/listener",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": networkID,
                "nodeID": nodeID,
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    listenerNum = response.data["count"];
                    listeners = response.data["listeners"];
                    $scope.liT += listenerNum;
                    Data.setLiT($scope.liT);
                } else {
                    listenerNum = 0;
                    listeners = [];
                }
                $scope.user.networks[netlistPos].nodes[nodelistPos].listenerNum = listenerNum;
                $scope.user.networks[netlistPos].nodes[nodelistPos].listeners = listeners;
                Data.setUser($scope.user);
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.chartReload = function (time, chartNum) {
        var tChart;
        if (chartNum == 0) {
            tChart = chart;
        } else if (chartNum == 1) {
            tChart = chart2;
        }

        if(shownDataT.length > 0) {
            tChart.unload();
        }
        if (Object.keys($scope.dataSel).length < 1) {
            $scope.popWarning("Please select some data types first!");
            return;
        }
        var d = new Date();
        if (time == 0) {
            d.setDate(d.getDate()-1);
        } else if (time == 1) {
            d.setDate(d.getDate()-7);
        }else if (time == 2) {
            d.setMonth(d.getMonth()-1);
        } else {
            d.setYear(d.getYear()-1);
        }
        var tString = d.toISOString(); //"2011-12-19T15:28:46.493Z"
        tString = tString.substring(0, tString.length - 5) + "+1000";
        var Tdata = [];
        for (var key in $scope.dataSel) {
            if ($scope.dataSel.hasOwnProperty(key) && $scope.dataSel[key] == true) {
                Tdata.push(key);
            }
        }
        shownDataT = Tdata;
        if ($scope.selNetworkID != -1 && $scope.selNodeID != -1) {
            $http({
                method : "POST",
                url : host + "/get/nodeDataT",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'Accept': "application/json; charset=utf-8",
                },
                data: {
                    "networkID": $scope.selNetworkID,
                    "nodeID": $scope.selNodeID,
                    "dataType": Tdata,
                    "timeStamp": tString
                },
                withCredentials: true
            }).then(function success(response) {
                if("success" in response.data) {
                    if (response.data["success"] == true) {
                        loadGraph(tChart, [response.data], Tdata, 'line', $scope.user.networks[$scope.networkSel].nodes[$scope.nodeSel].name);
                        for(var i=0;i<shownDataT.length;i++){
                            shownDataT[i]=$scope.user.networks[$scope.networkSel].nodes[$scope.nodeSel].name+":"+shownDataT[i];
                        }
                    } else {
                        $scope.popError(response.data["reason"]);
                    }
                } else {
                    $scope.popError("Response from server was malformed!");
                }
                $scope.submit = false;
            }, function error(response) {
                $scope.process401(response);
            });
        } else if ($scope.selNetworkID == -1 && $scope.selNodeID == -1) {
            $http({
                method : "POST",
                url : host + "/get/userDataT",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'Accept': "application/json; charset=utf-8",
                },
                data: {
                    "dataType": Tdata,
                    "timeStamp": tString
                },
                withCredentials: true
            }).then(function success(response) {
                if("success" in response.data) {
                    if (response.data["success"] == true) {
                        loadGraph(tChart, [response.data], Tdata, 'line', "User");
                        for(var i=0;i<shownDataT.length;i++){
                            shownDataT[i]="User:"+shownDataT[i];
                        }
                    } else {
                        $scope.popError(response.data["reason"]);
                    }
                } else {
                    $scope.popError("Response from server was malformed!");
                }
                $scope.submit = false;
            }, function error(response) {
                $scope.process401(response);
            });
        } else if ($scope.selNetworkID != -1 && $scope.selNodeID == -1) {
            $http({
                method : "POST",
                url : host + "/get/networkDataT",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'Accept': "application/json; charset=utf-8",
                },
                data: {
                    "networkID": $scope.selNetworkID,
                    "dataType": Tdata,
                    "timeStamp": tString
                },
                withCredentials: true
            }).then(function success(response) {
                if("success" in response.data) {
                    if (response.data["success"] == true) {
                        loadGraph(tChart, [response.data], Tdata, 'line', $scope.user.networks[$scope.networkSel].name);
                        for(var i=0;i<shownDataT.length;i++){
                            shownDataT[i]=$scope.user.networks[$scope.networkSel].name+":"+shownDataT[i];
                        }
                    } else {
                        $scope.popError(response.data["reason"]);
                    }
                } else {
                    $scope.popError("Response from server was malformed!");
                }
                $scope.submit = false;
            }, function error(response) {
                $scope.process401(response);
            });
        }

    };

    $scope.chartLoad = function (time, chartNum) {
        var tChart;
        if (chartNum == 0) {
            tChart = chart;
        } else if (chartNum == 1) {
            tChart = chart2;
        }

        if (Object.keys($scope.dataSel).length < 1) {
            $scope.popWarning("Please select some data types first!");
            return;
        }
        var d = new Date();
        if (time == 0) {
            d.setDate(d.getDate()-1);
        } else if (time == 1) {
            d.setDate(d.getDate()-7);
        }else if (time == 2) {
            d.setMonth(d.getMonth()-1);
        } else {
            d.setYear(d.getYear()-1);
        }
        var tString = d.toISOString(); //"2011-12-19T15:28:46.493Z"
        tString = tString.substring(0, tString.length - 5) + "+1000";
        var Tdata = [];
        for (var key in $scope.dataSel) {
            if ($scope.dataSel.hasOwnProperty(key) && $scope.dataSel[key] == true) {
                Tdata.push(key);
            }
        }
        if ($scope.selNetworkID != -1 && $scope.selNodeID != -1) {
            $http({
                method : "POST",
                url : host + "/get/nodeDataT",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'Accept': "application/json; charset=utf-8",
                },
                data: {
                    "networkID": $scope.selNetworkID,
                    "nodeID": $scope.selNodeID,
                    "dataType": Tdata,
                    "timeStamp": tString
                },
                withCredentials: true
            }).then(function success(response) {
                if("success" in response.data) {
                    if (response.data["success"] == true) {
                        loadGraph(tChart, [response.data], Tdata, 'line', $scope.user.networks[$scope.networkSel].nodes[$scope.nodeSel].name);
                    } else {
                        $scope.popError(response.data["reason"]);
                    }
                } else {
                    $scope.popError("Response from server was malformed!");
                }
                $scope.submit = false;
            }, function error(response) {
                $scope.process401(response);
            });
        } else if ($scope.selNetworkID == -1 && $scope.selNodeID == -1) {
            $http({
                method : "POST",
                url : host + "/get/userDataT",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'Accept': "application/json; charset=utf-8",
                },
                data: {
                    "dataType": Tdata,
                    "timeStamp": tString
                },
                withCredentials: true
            }).then(function success(response) {
                if("success" in response.data) {
                    if (response.data["success"] == true) {
                        loadGraph(tChart, [response.data], Tdata, 'line', "User");
                    } else {
                        $scope.popError(response.data["reason"]);
                    }
                } else {
                    $scope.popError("Response from server was malformed!");
                }
                $scope.submit = false;
            }, function error(response) {
                $scope.process401(response);
            });
        } else if ($scope.selNetworkID != -1 && $scope.selNodeID == -1) {
            $http({
                method : "POST",
                url : host + "/get/networkDataT",
                headers: {
                    'Content-Type': "application/json; charset=utf-8",
                    'Accept': "application/json; charset=utf-8",
                },
                data: {
                    "networkID": $scope.selNetworkID,
                    "dataType": Tdata,
                    "timeStamp": tString
                },
                withCredentials: true
            }).then(function success(response) {
                if("success" in response.data) {
                    if (response.data["success"] == true) {
                        loadGraph(tChart, [response.data], Tdata, 'line', $scope.user.networks[$scope.networkSel].name);
                    } else {
                        $scope.popError(response.data["reason"]);
                    }
                } else {
                    $scope.popError("Response from server was malformed!");
                }
                $scope.submit = false;
            }, function error(response) {
                $scope.process401(response);
            });
        }

    };

    $scope.loadData = function () {
        if ($scope.dataLoaded == false && comingFromLogin == true) {
            comingFromLogin = false;
            $scope.dataLoaded == true;
            $scope.updateMap();
            $scope.popInfo("Loading Data");
            $scope.viewNetworks();
            $scope.updateTime = getTime();
            Data.setUpdateTime($scope.updateTime);
            if (webSocket == null) {
                webSocket = io("https://t.th1nk.io:8866");
                webSocket.once('connect', function(){
                    webSocket.emit('room', $scope.user.uuid);
                });
                webSocket.on('chat message', function(msg){
                    $scope.updateFeed(msg);
                    return false;
                });
                webSocket.on('update', function(msg){
                    $scope.updateFeed(msg);
                });
                $interval(function(){
                    $scope.feed = Data.getFeed();
                    if ($('.feedbar').length > 0) {
                        $('.feedbar').mCustomScrollbar('scrollTo','bottom');
                    }
                },1000);
                $interval(function(){
                    $scope.updateMap();
                },60000);
            }
        }
    };

    $scope.updateMap = function () {
        window.paceOptions = {
            ajax: false
        };
        Pace.options.ajax = false;
        $http({
            method : "POST",
            url : host + "/get/nodeData",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": "4801",
                "nodeID": "403472985",
                "skip": "0",
                "limit": "1",
                "dataType": ["BAT", "CO2", "CO", "NO2", "HUMB", "O3", "TCA"]
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    var tr = "<tr><td>Description</td><td>East Lake Walk Sensor</td></tr>"

                    for (var key in response.data["data"][0]) {
                        tr += "<tr><td>" + key + "</td><td>" + response.data["data"][0][key] + "</td></tr>";
                    }
                    $scope.Node1.label.message = '<table class="table table-hover table-bordered table-condensed">' +
                        '<thead><tr><th bgcolor="#000000">Type</th><th bgcolor="#000000">Value</th></tr></thead><tbody>' +
                        tr + '</tbody></table>';
                    Data.setNode1($scope.Node1);
                } else {
                    $scope.popError(response.data["reason"]);
                }
                window.paceOptions = {
                    ajax: {
                        trackMethods: ["GET", "POST"],
                        trackWebSockets: false
                    },
                    restartOnPushState: false
                };
                Pace.options.ajax = {"trackMethods":["GET","POST"],"trackWebSockets":false,"ignoreURLs":[]};
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
        $http({
            method : "POST",
            url : host + "/get/nodeData",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": "4801",
                "nodeID": "403361490",
                "skip": "0",
                "limit": "1",
                "dataType": ["BAT", "CO2", "CO", "NO2", "HUMB", "O3", "TCA"]
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    var tr = "<tr><td>Description</td><td>North Lake Walk Sensor</td></tr>"

                    for (var key in response.data["data"][0]) {
                        tr += "<tr><td>" + key + "</td><td>" + response.data["data"][0][key] + "</td></tr>";
                    }
                    $scope.Node2.label.message = '<table class="table table-hover table-bordered table-condensed">' +
                        '<thead><tr><th bgcolor="#000000">Type</th><th bgcolor="#000000">Value</th></tr></thead><tbody>' +
                        tr + '</tbody></table>';
                    Data.setNode2($scope.Node2);
                } else {
                    $scope.popError(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
        $http({
            method : "POST",
            url : host + "/get/nodeData",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": "4801",
                "nodeID": "403371482",
                "skip": "0",
                "limit": "1",
                "dataType": ["BAT", "CO2", "CO", "NO2", "HUMB", "O3", "TCA"]
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    var tr = "<tr><td>Description</td><td>Lake Bus Stop Sensor</td></tr>"

                    for (var key in response.data["data"][0]) {
                        tr += "<tr><td>" + key + "</td><td>" + response.data["data"][0][key] + "</td></tr>";
                    }
                    $scope.Node3.label.message = '<table class="table table-hover table-bordered table-condensed">' +
                        '<thead><tr><th bgcolor="#000000">Type</th><th bgcolor="#000000">Value</th></tr></thead><tbody>' +
                        tr + '</tbody></table>';
                    Data.setNode3($scope.Node3);
                } else {
                    $scope.popError(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
        $http({
            method : "POST",
            url : host + "/get/nodeData",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": "4801",
                "nodeID": "403458853",
                "skip": "0",
                "limit": "1",
                "dataType": ["BAT", "CO2", "CO", "NO2", "HUMB", "O3", "TCA"]
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    var tr = "<tr><td>Description</td><td>West Lake Bike Way Sensor</td></tr>"

                    for (var key in response.data["data"][0]) {
                        tr += "<tr><td>" + key + "</td><td>" + response.data["data"][0][key] + "</td></tr>";
                    }
                    $scope.Node4.label.message = '<table class="table table-hover table-bordered table-condensed">' +
                        '<thead><tr><th bgcolor="#000000">Type</th><th bgcolor="#000000">Value</th></tr></thead><tbody>' +
                        tr + '</tbody></table>';
                    Data.setNode4($scope.Node4);
                } else {
                    $scope.popError(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
        $http({
            method : "POST",
            url : host + "/view/node",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": "6"
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    for (var i = 0; i < response.data.nodes.length; i++) {
                        if (response.data.nodes[i].nodeID == "1") {
                            $scope.NodeTest.lat = parseFloat(response.data.nodes[i].lat);
                            $scope.NodeTest.lon = parseFloat(response.data.nodes[i].lon);
                        }
                    }
                    Data.setNodeTest($scope.NodeTest);
                } else {
                    $scope.popError(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
        $http({
            method : "POST",
            url : host + "/get/nodeData",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": "6",
                "nodeID": "1",
                "skip": "0",
                "limit": "1",
                "dataType": ["speed","temperature"]
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    var tr = "<tr><td>Description</td><td>Test node in Network:Home Node:N1</td></tr>"

                    for (var key in response.data["data"][0]) {
                        tr += "<tr><td>" + key + "</td><td>" + response.data["data"][0][key] + "</td></tr>";
                    }
                    $scope.NodeTest.label.message = '<table class="table table-hover table-bordered table-condensed">' +
                        '<thead><tr><th bgcolor="#000000">Type</th><th bgcolor="#000000">Value</th></tr></thead><tbody>' +
                        tr + '</tbody></table>';
                    Data.setNodeTest($scope.NodeTest);
                } else {
                    $scope.popError(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.registerNetwork = function () {
        $scope.submit = true;
        $http({
            method : "POST",
            url : host + "/register/network",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": $scope.rnID,
                "name": $scope.rnName,
                "description": $scope.rnDes
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    $scope.popSuccess("Registration was successful!");
                    $scope.viewNetworks();
                } else {
                    $scope.popWarning(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.registerNode = function () {
        $scope.submit = true;
        $http({
            method : "POST",
            url : host + "/register/node",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": $scope.rnoNetwork,
                "nodeID": $scope.rnoID,
                "name": $scope.rnoName,
                "description": $scope.rnoDes,
                "connectedTo": $scope.rnoConnect,
                "lon": $scope.rnoLon,
                "lat": $scope.rnoLat,
                "lastSyncTime": "2016-10-01T00:00:00+1000",
                "inactivityLimit": "30"
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    $scope.popSuccess("Registration was successful!");
                    $scope.viewNetworks();
                } else {
                    $scope.popWarning(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.registerTrigger = function () {
        $scope.submit = true;
        $http({
            method : "POST",
            url : host + "/register/trigger",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": $scope.user.networks[$scope.rtrNetwork].networkID,
                "nodeID": $scope.user.networks[$scope.rtrNetwork].nodes[$scope.rtrNode].nodeID,
                "dataType": $scope.rtrType,
                "compare": $scope.rtrComp,
                "script": $scope.rtrScript,
                "value": $scope.rtrValue
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    $scope.popSuccess("Registration was successful!");
                    $scope.viewNetworks();
                } else {
                    $scope.popWarning(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    $scope.registerListener = function () {
        $scope.submit = true;
        $http({
            method : "POST",
            url : host + "/register/listener",
            headers: {
                'Content-Type': "application/json; charset=utf-8",
                'Accept': "application/json; charset=utf-8",
            },
            data: {
                "networkID": $scope.user.networks[$scope.rliNetwork].networkID,
                "nodeID": $scope.user.networks[$scope.rliNetwork].nodes[$scope.rliNode].nodeID,
                "script": $scope.rliScript
            },
            withCredentials: true
        }).then(function success(response) {
            if("success" in response.data) {
                if (response.data["success"] == true) {
                    $scope.popSuccess("Registration was successful!");
                    $scope.viewNetworks();
                } else {
                    $scope.popWarning(response.data["reason"]);
                }
            } else {
                $scope.popError("Response from server was malformed!");
            }
            $scope.submit = false;
        }, function error(response) {
            $scope.process401(response);
        });
    };

    if ($state.current.name == 'test.login') {
        comingFromLogin = true;
        $scope.authCheck();
    }

    if ($state.current.name == 'thingular.livemap') {

    }

    if ($state.current.name == 'thingular.dashboard') {
        setTimeout(function(){
            chart = drawGraph(init_data,[''],'line',null, "chartd");
            chart2 = drawGraph(init_data,[''],'line',null, "chartd2");
        },1000);
        $scope.loadData();
    }

    $scope.updateFeed = function (msg) {
        $scope.feed.push(msg);
        Data.setFeed($scope.feed);
        $scope.$apply();
    };

    if ($state.current.name == 'thingular.datafeed') {
        $('#chat_app').submit(function(){
            webSocket.emit('chat message', $('#chat_input').val());
            $('#chat_input').val('');
            return false;
        });
    }

    if ($state.current.name == 'thingular.script') {
        if (term == null && terminalContainer == null && termSocket == null) {
            terminalContainer = document.getElementById('terminal-container');
            term = new Terminal({ cursorBlink: true });
            term.open(terminalContainer);
            term.fit();

            termSocket = io.connect("https://t.th1nk.io:8867");
            termSocket.on('connect', function() {
                term.write('\r\n************************************************************\r\n');
                term.write('\r\nConnected to Thingular ThinkScript Python Shell.\r\n');
                term.write('\r\nPlease be aware that for security reasons, the keyword "import" is banned.\r\n');
                term.write('\r\nAdditional libraries can be imported upon request.\r\n');
                term.write('\r\nIf there is a problem, please contact system admin at patrick@th1nk.io!\r\n');
                term.write('\r\n************************************************************\r\n');
                // Browser -> Backend
                term.on('data', function(data) {
                    termSocket.emit('data', data);
                });

                // Backend -> Browser
                termSocket.on('data', function(data) {
                    term.write(data);
                });

                termSocket.on('disconnect', function() {
                    term.write('\r\n*** Disconnected from Thingular, please refresh page to reconnect***\r\n');
                });
            });
        }
        else {
            termSocket.emit('data', "exit()");
            termSocket.emit('data', String.fromCharCode(13));
            setTimeout(function () {
                terminalContainer = document.getElementById('terminal-container');
                term = new Terminal({cursorBlink: true});
                term.open(terminalContainer);
                term.fit();

                termSocket = io.connect("https://t.th1nk.io:8867");
                termSocket.on('connect', function () {
                    term.write('\r\n************************************************************\r\n');
                    term.write('\r\nConnected to Thingular ThinkScript Python Shell.\r\n');
                    term.write('\r\nPlease be aware that for security reasons, the keyword "import" is banned.\r\n');
                    term.write('\r\nAdditional libraries can be imported upon request.\r\n');
                    term.write('\r\nIf there is a problem, please contact system admin at patrick@th1nk.io!\r\n');
                    term.write('\r\n************************************************************\r\n');
                    // Browser -> Backend
                    term.on('data', function (data) {
                        termSocket.emit('data', data);
                    });

                    // Backend -> Browser
                    termSocket.on('data', function (data) {
                        term.write(data);
                    });

                    termSocket.on('disconnect', function () {
                        term.write('\r\n*** Disconnected from Thingular, please refresh page to reconnect***\r\n');
                    });
                });
            }, 1000);
        }
    }

}