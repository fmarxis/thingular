/**
 *
 * alertsCtrl
 *
 */

angular
    .module('thingular')
    .controller('alertsCtrl', alertsCtrl);

function alertsCtrl($scope, toastr, toastrConfig){

    angular.extend(toastrConfig, {
        newestOnTop: false,
        positionClass: 'toast-bottom-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        progressBar: true
    });

    $scope.popInfo = function (msg) {
        if (msg == null) {
            msg = "Thingular";
        }
        toastr.info('Info - ' + msg, {});
    };

    $scope.popSuccess = function () {
        toastr.success('Success - ', {});
    };

    $scope.popWarning = function () {
        toastr.warning('Warning - ', {});
    };

    $scope.popError = function () {
        toastr.error('Error - ', {});
    };



}
