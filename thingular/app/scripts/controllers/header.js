/**
 *
 * headerCtrl
 *
 */

angular
    .module('thingular')
    .controller('headerCtrl', headerCtrl);

angular
    .module('thingular')
    .service('userInfo', function () {
        var userInfo = {firstName: "Thingular", lastName: "Test", email: "admin@th1nk.io", uuid: "0000-0000-0000-0000-0000"};

        return {
            getUserInfo: function () {
                return userInfo;
            },
            setUserInfo: function(value) {
                userInfo = value;
            }
        };
    });

function headerCtrl($scope, userInfo){
    $scope.updateProfile = function () {
        $scope.profile_name = user["firstName"] + " " + user["lastName"];
    };
    $scope.user = userInfo.getUserInfo()
    $scope.profile_name = $scope.user['firstName'] + " " + $scope.user['lastName'];
    $scope.profile_email = $scope.user['email'];
}