//Thingular Javascript Functions
var host = "https://t.th1nk.io/api";
var networkCount;
var networks;
var nodeCount;
var nodes;
var triggerCount;
var triggers;
var listenerNum;
var listeners;
var nodeTotal;
var triggerTotal;
var listenerTotal;
var dataLoaded = false;
var comingFromLogin = false;
var webSocket = null;
var loginStatus = false;
var chart, chart2;
var init_data = [
    {
        "success": true,
        "nodeID": "1",
        "data": [{

        }]
    }
];
var shownDataT = [];
var terminalContainer = null;
var term = null;
var termSocket = null;
var refresh = false;

function getTime() {
    var d = new Date();
    var hr = d.getHours();
    var min = d.getMinutes();
    if (min < 10) {
        min = "0" + min;
    }
    var ampm = hr < 12 ? "am" : "pm";
    return  hr + ":" + min + ampm;
}