/**
 * Thingular Web Config
 *
 */

function configState($stateProvider, $urlRouterProvider, $compileProvider) {

    // Optimize load start with remove binding information inside the DOM element
    $compileProvider.debugInfoEnabled(true);

    // Set default state
    $urlRouterProvider.otherwise("/test/login");
    $stateProvider
    // Main content
        .state('test', {
            abstract: true,
            url: "/test",
            templateUrl: "views/common/blank.html",
            data: {
                specialClass: 'blank',
                requireLogin: false
            }
        })
        .state('test.login', {
            url: "/login",
            templateUrl: "views/login.html",
            data: {
                pageTitle: 'Login'
            }
        })
        .state('test.signup', {
            url: "/signup",
            templateUrl: "views/signup.html",
            data: {
                pageTitle: 'Signup'
            }
        })
        .state('test.reset', {
            url: "/reset",
            templateUrl: "views/reset.html",
            data: {
                pageTitle: 'Password Reset'
            }
        })
        .state('thingular', {
            abstract: true,
            url: "",
            templateUrl: "views/common/content.html",
            data: {
                requireLogin: true,
            },
        })
        .state('thingular.dashboard', {
            url: "/dashboard",
            templateUrl: "views/dashboard.html",
            data: {
                pageTitle: 'Dashboard'
            }
        })
        .state('thingular.datafeed', {
            url: "/datafeed",
            templateUrl: "views/datafeed.html",
            data: {
                pageTitle: 'Data Feed'
            }
        })
        .state('thingular.livemap', {
            url: "/livemap",
            templateUrl: "views/livemap.html",
            data: {
                pageTitle: 'Live Map'
            }
        })
        .state('thingular.profile', {
            url: "/profile",
            templateUrl: "views/profile.html",
            data: {
                pageTitle: 'User Profile'
            }
        })
        //Networks
        .state('thingular.networks', {
            url: "/networks",
            template: "<div ui-view></div>"
        })
        .state('thingular.networks.view', {
            url: "/view",
            templateUrl: "views/networks/view.html",
            data: {
                pageTitle: 'View Networks'
            }
        })
        .state('thingular.networks.update', {
            url: "/update",
            templateUrl: "views/networks/update.html",
            data: {
                pageTitle: 'Update Networks'
            }
        })
        .state('thingular.networks.create', {
            url: "/create",
            templateUrl: "views/networks/create.html",
            data: {
                pageTitle: 'Create Networks'
            }
        })
        //Nodes
        .state('thingular.nodes', {
            url: "/nodes",
            template: "<div ui-view></div>"
        })
        .state('thingular.nodes.view', {
            url: "/view",
            templateUrl: "views/nodes/view.html",
            data: {
                pageTitle: 'View Nodes'
            }
        })
        .state('thingular.nodes.update', {
            url: "/update",
            templateUrl: "views/nodes/update.html",
            data: {
                pageTitle: 'Update Nodes'
            }
        })
        .state('thingular.nodes.create', {
            url: "/create",
            templateUrl: "views/nodes/create.html",
            data: {
                pageTitle: 'Create Nodes'
            }
        })
        //Analysis
        .state('thingular.script', {
            url: "/script",
            templateUrl: "views/script.html",
            data: {
                pageTitle: 'ThinkScript'
            }
        })
        .state('thingular.triggers', {
            url: "/triggers",
            template: "<div ui-view></div>"
        })
        .state('thingular.triggers.view', {
            url: "/view",
            templateUrl: "views/triggers/view.html",
            data: {
                pageTitle: 'View Triggers'
            }
        })
        .state('thingular.triggers.update', {
            url: "/update",
            templateUrl: "views/triggers/update.html",
            data: {
                pageTitle: 'Update Triggers'
            }
        })
        .state('thingular.triggers.create', {
            url: "/create",
            templateUrl: "views/triggers/create.html",
            data: {
                pageTitle: 'Create Triggers'
            }
        })
        .state('thingular.listeners', {
            url: "/listeners",
            template: "<div ui-view></div>"
        })
        .state('thingular.listeners.view', {
            url: "/view",
            templateUrl: "views/listeners/view.html",
            data: {
                pageTitle: 'View Listeners'
            }
        })
        .state('thingular.listeners.update', {
            url: "/update",
            templateUrl: "views/listeners/update.html",
            data: {
                pageTitle: 'Update Listeners'
            }
        })
        .state('thingular.listeners.create', {
            url: "/create",
            templateUrl: "views/listeners/create.html",
            data: {
                pageTitle: 'Create Listeners'
            }
        })
}

angular
    .module('thingular')
    .config(configState)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            var requireLogin = toState.data.requireLogin;
            if (requireLogin && (loginStatus == false)) {
                event.preventDefault();
                $rootScope.$state.go('test.login');
            }
        });
    });