var nodalData = {
  "success": true,
  "count": 4,
  "nodes": [
    {
      "lastSyncTime": "2016-10-01T00:00:00+1000",
      "name": "Test Node 1",
      "description": "Test Node",
      "lon": "153.015821",
      "nodeID": "1",
      "lat": "-27.499624",
      "inactivityLimit": "30"
    },
    {
      "lastSyncTime": "2016-10-01T00:00:00+1000",
      "name": "Test Node 2",
      "description": "Test Node",
      "lon": "153.017025",
      "nodeID": "2",
      "lat": "-27.499507",
      "inactivityLimit": "30"
    },
    {
      "lastSyncTime": "2016-10-01T00:00:00+1000",
      "name": "Test Node 3",
      "description": "Test Node",
      "lon": "153.016569",
      "nodeID": "3",
      "lat": "-27.498637",
      "inactivityLimit": "30"
    },
    {
      "lastSyncTime": "2016-10-01T00:00:00+1000",
      "name": "Test Node 4",
      "description": "Test Node",
      "lon": "153.017208",
      "nodeID": "4",
      "lat": "-27.497880",
      "inactivityLimit": "30"
    }
  ]
};