



/**
*** Arguments
*** 
*** div
***     String of the div to add the map to
***     e.g. "map"
*** 
*** nodalData
***     {
***         "success": true,
***         "count": 4,
***         "nodes": [
***             {
***                 "lastSyncTime": "2016-10-01T00:00:00+1000",
***                 "name": "Test Node 1",
***                 "description": "Test Node",
***                 "lon": "153.015821",
***                 "nodeID": "1",
***                 "lat": "-27.499624",
***                 "inactivityLimit": "30"
***             },
***             {
***                 ...
***             },
***             ...
***         ]
***     }
*** 
*** dataArray
***     [
*** 
***         {
***             "success": true,
***             "nodeID": "1",
***             "data": [
***                 {
***                     "NO2": "7",
***                     "HUM": "54",
***                     "TCA": "43",
***                     "O2": "4",
***                     "CO2": "211",
***                     "CO": "4",
***                     "batteryLevel": "22",
***                     "nodeID": "1",
***                     "timeStamp": "2016-10-01T17:00:00+1000"
***                 },
***                 {
***                     ...
***                 },
***                 ...
***             ]
***         },
***         {
***             ...
***         },
***         ...
***     ]
***
*** sensorChosen
***     e.g. "TCA"
*** 
*** 
**/
function startContours (div, nodalData, dataArray, sensorChosen) {

    map = drawMap(div);

    if (nodalData["success"] != true) {
        return;
    }

    for (var i = 0; i < nodalData["nodes"].length; ++i) {
        var arg1 = parseFloat(nodalData["nodes"][i]["lat"]);
        var arg2 = parseFloat(nodalData["nodes"][i]["lon"]);

        var marker = L.marker([arg1, arg2]).addTo(map);
    }

    if (dataArray[0]["success"] != true) {
        return;
    }

    var organisedData = organiseData(dataArray, null, [sensorChosen], false, true);

    if (organisedData == null) {
        return;
    }

    drawContours(div, map, organisedData, nodalData);
}