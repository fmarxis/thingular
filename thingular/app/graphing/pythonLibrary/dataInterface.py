#!/usr/bin/env python
"""
SYNOPSIS

    TODO helloworld [-h,--help] [-v,--verbose] [--version]

DESCRIPTION

    TODO This describes how to use this script. This docstring
    will be printed by the script if there is an error or
    if the user requests help (-h or --help).

EXAMPLES

    TODO: Show some examples of how to use this script.

EXIT STATUS

    TODO: List exit codes

AUTHOR

    Matthew Baldwin <matthew.baldwin@uqconnect.edu.au>

VERSION

    0.1
"""

import requests
import json
import datetime


GOOD_EXIT = 0
ERROR_ESTABLISHING_CONNECTION = 1
ERROR_INTERNAL = 2
ERROR_REQUESTING_FROM_SERVER = 3


url = "https://t.th1nk.io"
headers = {'Content-type': 'application/json', 'Accept': 'application/json'}

session = requests.Session()



def check_response(response):

    if response.status_code != 200:
        return False

    return True


def create_dictionary(keys, values):

    if (not isinstance(keys, list)) or (not isinstance(values, list)):
        return None

    if (len(keys) != len(values)):
        return None

    newDictionary = {}

    i = 0
    while i < len(keys):

        newDictionary[keys[i]] = values[i]

        i += 1

    return newDictionary


def create_user(emailAddress, password, firstName, lastName):
    
    innerDictionaryKeys = ["email", "password", "firstName", "lastName"]
    innerDictionaryValues = [emailAddress, password, firstName, lastName]
    innerDictionary = create_dictionary(innerDictionaryKeys, innerDictionaryValues)
    if innerDictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    fullDictionary = create_dictionary(["user"], [innerDictionary])
    if fullDictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(fullDictionary)

    try:
        response = session.post(url + "/api/user/signup",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def authenticate_user(emailAddress, password, remember):

    innerDictionaryKeys = ["email", "password", "remember"]
    innerDictionaryValues = [emailAddress, password, remember]
    innerDictionary = create_dictionary(innerDictionaryKeys, innerDictionaryValues)
    if innerDictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    fullDictionary = create_dictionary(["user"], [innerDictionary])
    if fullDictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(fullDictionary)

    try:
        response = session.post(url + "/api/user/signin",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def change_password(oldPassword, newPassword, token, UUID):

    dictionaryKeys = ["password", "newPassword", "token", "UUID"]
    dictionaryValues = [oldPassword, newPassword, token, UUID]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/user/update/password",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def get_time():

    try:
        response = session.get(url + "/api/utils/time",
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return datetime.datetime.strptime(response.text[:-5], "%Y-%m-%dT%H:%M:%S")


def register_network(networkID, networkName, networkDescription, token, UUID):

    dictionaryKeys = ["networkID", "name", "description", "token", "UUID"]
    dictionaryValues = [networkID, networkName, networkDescription, token, UUID]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/register/network",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def view_networks(token, UUID):

    dictionaryKeys = ["token", "UUID"]
    dictionaryValues = [token, UUID]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/view/network",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def update_network_API_key(networkID, token, UUID):

    dictionaryKeys = ["networkID", "token", "UUID"]
    dictionaryValues = [networkID, token, UUID]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/update/network/key",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def register_node(networkID, nodeID, nodeName, nodeDescription, connectedTo,
    latitude, longitude, lastSyncTime, inactivityLimit, token, UUID):

    dictionaryKeys = ["networkID", "nodeID", "name", "description",
                        "connectedTo", "lon", "lat", "lastSyncTime",
                        "inactivityLimit", "token", "UUID"]
    dictionaryValues = [networkID, nodeID, nodeName, nodeDescription,
                        connectedTo, longitude, latitude, lastSyncTime,
                        inactivityLimit, token, UUID]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/register/node",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def view_nodes(networkID, token, UUID):

    dictionaryKeys = ["networkID", "token", "UUID"]
    dictionaryValues = [networkID, token, UUID]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/view/node",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def sync_data(nodeID, APIKey, timeStamp, data):

    dictionaryKeys = ["nodeID", "APIKey", "timeStamp"]
    dictionaryValues = [nodeID, APIKey, timeStamp]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    dictionary.update(data)

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/sync",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def view_all_node_data(networkID, nodeID, token, UUID):

    dictionaryKeys = ["networkID", "nodeID", "token", "UUID"]
    dictionaryValues = [networkID, nodeID, token, UUID]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/data/node",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def view_all_network_data(networkID, token, UUID):

    dictionaryKeys = ["networkID", "token", "UUID"]
    dictionaryValues = [networkID, token, UUID]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/data/network",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()


def view_all_user_data(token, UUID):

    dictionaryKeys = ["token", "UUID"]
    dictionaryValues = [token, UUID]
    dictionary = create_dictionary(dictionaryKeys, dictionaryValues)
    if dictionary is None:
        print ("Internal Error in dictionary creation.  Error Code... {0}".format(ERROR_INTERNAL))
        return ERROR_INTERNAL

    jsonDictionary = json.dumps(dictionary)

    try:
        response = session.post(url + "/api/data/user",
                        data = jsonDictionary,
                        headers = headers)
    except:
        print ("Couldn't establish a connection.  Error Code... {0}".format(ERROR_ESTABLISHING_CONNECTION))
        return ERROR_ESTABLISHING_CONNECTION

    if not check_response(response):
        print ("Error making request.  Error Code... {0}".format(ERROR_REQUESTING_FROM_SERVER))
        return ERROR_REQUESTING_FROM_SERVER

    return response.json()