/**
*** Arguments
*** 
*** data
***     An Array of objects containing all the datas.
***     e.g. [{
***     
***         "success": true,
***         "nodeID": "1",
***         "data": [
***             {
***                 "NO2": "7",
***                 "HUM": "54",
***                 "TCA": "43",
***                 "O2": "4",
***                 "CO2": "211",
***                 "CO": "4",
***                 "batteryLevel": "22",
***                 "nodeID": "1",
***                 "timeStamp": "2016-10-01T17:00:00+1000"
***             },
***             {
***                 ...
***             },
***             ...
***         ]
***     }]
***
*** 
*** sensorsChosen
***     Array of strings representing the sensor(s) to get data from.
***     e.g. ["BAT"]
*** 
**/
function drawHeatMap (data, sensorsChosen) {

    var organisedData = organiseData(data, null, sensorsChosen, false);

    if (organisedData == null) {
        return null;
    }

    var xAxisData = generateHours();
    var yAxisData = generateDays(organisedData["xAxisData"][0].getDay());

    var heatmapData = refineOrganisedData(organisedData, xAxisData, yAxisData);


    var data = [
      {
        z: heatmapData,
        x: xAxisData,
        y: yAxisData,
        type: 'heatmap'
      }
    ];

    Plotly.newPlot('chart', data);

}