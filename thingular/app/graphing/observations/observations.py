
# with open('../TestData/pythonjson.json') as data_file:    
#     data = json.load(data_file)

#!/usr/bin/env python

import os
import sys
import json

GOOD_EXIT = 0
ARGUMENT_MISMATCH_ERROR = 1
FILE_NOT_FOUND_ERROR = 2
JSON_PARSING_ERROR = 3
JSON_OBJECT_INVALID_ERROR = 4

def read_file(directory):
    try:
        jsonFile = open(directory, 'rb')
    except:
        return None

    return jsonFile


def parse_json(file):
    try:
        jsonObject = json.load(file)
    except:
        return None

    return jsonObject


def json_object_valid(jsonObject):
    return jsonObject[0]["success"]


def check_json_key(data, key):

    if key in data:
        return True
    else:
        return False


def temperature_observation(data, temperatureKey):

    temperatureObservation = "Temperature Observation\n"

    if not check_json_key(data, temperatureKey):
        temperatureObservation += "Not enough data\n\n"
        return temperatureObservation

    temperatureReading = float(data[temperatureKey])

    if temperatureReading < 15:
        temperatureObservation += "Cold"
    elif temperatureReading < 30:
        temperatureObservation += "Mild"
    else:
        temperatureObservation += "Hot"

    temperatureObservation += " ({0}C)\n\n".format(temperatureReading)

    return temperatureObservation


def rain_mugginess_observation(data, temperatureKey, humidityKey):

    rainObservation = "Rain & Mugginess Observation\n"

    if (not check_json_key(data, temperatureKey)) or (not check_json_key(data, humidityKey)):
        rainObservation += "Not enough data\n\n"
        return rainObservation

    temperatureReading = float(data[temperatureKey])
    humidityReading = float(data[humidityKey])

    if temperatureReading > 25 and humidityReading > 75:
        rainObservation += "Possible Rain...\n"
        rainObservation += "Possible feeling of mugginess ...\n"
    elif temperatureReading > 25 and humidityReading <= 40:
        rainObservation += "May be dry outside...\n"
        rainObservation += "If high winds are present, the likelihood of fire dangers is high\n"
    else:
        rainObservation += "Should be clear of both...\n"

    rainObservation += "Due to temperatures of ({0}C) and a humidity percentage of ({1}%)\n\n".format(temperatureReading, humidityReading)

    return rainObservation


def pollution_observation(data, carbonMonoxideKey):

    pollutionObservation = "Pollution Observation\n"

    if not check_json_key(data, carbonMonoxideKey):
        pollutionObservation += "Not enough data\n\n"
        return pollutionObservation

    carbonMonoxideReading = float(data[carbonMonoxideKey])

    if carbonMonoxideReading > 4:
        pollutionObservation += "Pollution in the area is higher than average\n"
    else:
        pollutionObservation += "Pollution in the area is safe (lower than avaerge)\n"

    pollutionObservation += "Carbon Monoxide (CO) measured at {0}ppm\n\n".format(carbonMonoxideReading)

    return pollutionObservation


def air_quality_observations(data, nitrogenDioxideKey, carbonDioxideKey, ozoneKey):

    no2Out = False
    co2Out = False
    o3Out = False

    nitrogenDioxideReading = None
    carbonDioxideReading = None
    ozoneReading = None

    airQualityObservation = "Air Quality Observation\n"

    if not check_json_key(data, nitrogenDioxideKey):
        no2Out = True
    else:
        nitrogenDioxideReading = float(data[nitrogenDioxideKey])

    if not check_json_key(data, carbonDioxideKey):
        co2Out = True
    else:
        carbonDioxideReading = float(data[carbonDioxideKey])

    if not check_json_key(data, ozoneKey):
        o3Out = True
    else:
        ozoneReading = float(data[ozoneKey])

    if no2Out and co2Out and o3Out:
        airQualityObservation += "Not enough data\n\n"
        return airQualityObservation

    if nitrogenDioxideReading is not None and nitrogenDioxideReading > 0.1:
        airQualityObservation += "Beware of the quality in the area.  Breathing may difficult for those with respiratory problems\n"
        airQualityObservation += "Nitrogen Dioxide, Carbon Dioxide, and/or Ground Level Ozone readings are high\n"
    elif carbonDioxideReading is not None and carbonDioxideReading > 0.2:
        airQualityObservation += "Beware of the quality in the area.  Breathing may difficult for those with respiratory problems\n"
        airQualityObservation += "Nitrogen Dioxide, Carbon Dioxide, and/or Ground Level Ozone readings are high\n"
    elif ozoneReading is not None and ozoneReading > 0.08:
        airQualityObservation += "Beware of the quality in the area.  Breathing may difficult for those with respiratory problems\n"
        airQualityObservation += "Nitrogen Dioxide, Carbon Dioxide, and/or Ground Level Ozone readings are high\n"
    else:
        airQualityObservation += "Air quality in this area is quite good\n"

    return airQualityObservation


"""

    Arguments

    0. Script Name

    1. File
        File containing the json object to be parsed

    2. Temp
        The name of the key which holds temp data

    3. Humidity

    4. CO

    5. NO2

    6. CO2

    7. O3


"""
def main(arguments):

    if len(arguments) != 8:
        return ARGUMENT_MISMATCH_ERROR

    jsonFile = read_file(arguments[1])
    if jsonFile is None:
        return FILE_NOT_FOUND_ERROR

    jsonObject = parse_json(jsonFile)
    if jsonObject is None:
        jsonFile.close()
        return JSON_PARSING_ERROR

    jsonFile.close()

    if not json_object_valid(jsonObject):
        return JSON_OBJECT_INVALID_ERROR

    data = jsonObject[0]["data"][0]

    observations = "Observations\n\n"

    temperatureObservations = temperature_observation(data, arguments[2])
    if temperatureObservations is not None:
        observations += temperatureObservations

    rainMigginessObservations = rain_mugginess_observation(data, arguments[2], arguments[3])
    if rainMigginessObservations is not None:
        observations += rainMigginessObservations

    pollutionObservations = pollution_observation(data, arguments[4])
    if pollutionObservations is not None:
        observations += pollutionObservations

    airQualityObservations = air_quality_observations(data, arguments[5], arguments[6], arguments[7])
    if airQualityObservations is not None:
        observations += airQualityObservations

    print(observations)

    return GOOD_EXIT

if __name__ == '__main__':
    exitStatus = main(sys.argv)

    print ('Exiting with status... {0}.'.format(exitStatus))
    sys.exit(exitStatus)