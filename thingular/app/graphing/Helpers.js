/////////////
// HELPERS //
/////////////

function areGivenArrayLengthsEqual (xAxisData, data) {

    var arrayOne = [];
    var arrayTwo = [];

    arrayOne.push.apply(arrayOne, xAxisData);
    arrayTwo.push.apply(arrayTwo, data);

    if (arrayOne.length !== arrayTwo.length) {
        return false;
    } else {
        return true;
    }
}

// Tested
function xAxisDataContainsNullOrDuplicates (xAxisData) {

    var array = [];
    array.push.apply(array, xAxisData);

    var valuesSoFar = [];

    for (var i = 0; i < array.length; ++i) {

        var value = array[i];

        if (value == null) {
            return true;
        }

        if (valuesSoFar.indexOf(value) !== -1) {
            return true;
        }

        valuesSoFar.push(value);

    }

    return false;
}

// Tested
function organiseData (data, sensorsChosen, titlePrepend) {
    organisedData = {
        "xAxisData": [],
        "yAxisData": [],
        "xs":{}
    };

    for (var i = 0; i < data.length; ++i) {

        if (data[i]["success"] != true) {
            return null;
        }

        organisedData = iterateThroughData(organisedData, data[i]["data"], sensorsChosen, titlePrepend);
    }

    return organisedData;
}

function iterateThroughData (organisedData, data, sensorsChosen, titlePrepend) {
    for (var i = 0 ; i < sensorsChosen.length; i++) {
        organisedData.xAxisData.push(new Array());
        organisedData.yAxisData.push(new Array());
    }

    for (var j = data.length - 1; j >= 0; j--) {
        var count = 0;
        for (var key in data[j]) {
            if (data[j].hasOwnProperty(key)) count++;
        }
        if (count > 0) {
            for (var i = 0 ; i < sensorsChosen.length; i++) {
                if (data[j].hasOwnProperty(sensorsChosen[i]) && data[j][sensorsChosen[i]] != null) {
                    organisedData.xAxisData[i].push(data[j]["timeStamp"].substr(0,19)+'.00Z');
                    organisedData.yAxisData[i].push(parseFloat(data[j][sensorsChosen[i]]));
                }
            }
        }
    }

    if(titlePrepend != false) {
        for (var i = 0 ; i < sensorsChosen.length; i++) {
            var sensorString = titlePrepend+":"+sensorsChosen[i];
            organisedData["xAxisData"][i].unshift("x"+sensorString);
            organisedData["yAxisData"][i].unshift(sensorString);
            organisedData["xs"][sensorString] = "x"+sensorString;
        }
    } else {
        for (var i = 0 ; i < sensorsChosen.length; i++) {
            var sensorString = sensorsChosen[i];
            organisedData["xAxisData"][i].unshift("x"+sensorString);
            organisedData["yAxisData"][i].unshift(sensorString);
            organisedData["xs"][sensorString] = "x"+sensorString;
        }
    }

    return organisedData;
}

function getDateIndexInArray (dateArray, newDate) {

    for (var i = 0; i < dateArray.length; ++i) {

        var currentDate = dateArray[i].getTime();

        if (currentDate === newDate) {
            return i;
        }

    }

    return -1;
}

// Tested
function createDataTitle(nodeID, sensorChosen) {
    
    return "Node " + nodeID + " " + sensorChosen;
}

function generateHours () {

    var hours = [];

    for (var i = 0; i < 24; ++i) {
        if (i < 10) {
            var hourString = '0' + i.toString();
        } else {
            var hourString = i.toString();
        }
        hours.push(hourString);
    }

    return hours;
}

function generateDays(dayOne) {

    var days = [];

    if (dayOne === 0) {
        days = ['SUN', 'MON', 'TUE', 'WED', 'THUR', 'FRI', 'SAT'];
    } else if (dayOne === 1) {
        days = ['MON', 'TUE', 'WED', 'THUR', 'FRI', 'SAT', 'SUN'];
    } else if (dayOne === 2) {
        days = ['TUE', 'WED', 'THUR', 'FRI', 'SAT', 'SUN', 'MON'];
    } else if (dayOne === 3) {
        days = ['WED', 'THUR', 'FRI', 'SAT', 'SUN', 'MON', 'TUE'];
    } else if (dayOne === 4) {
        days = ['THUR', 'FRI', 'SAT', 'SUN', 'MON', 'TUE', 'WED'];
    } else if (dayOne === 5) {
        days = ['FRI', 'SAT', 'SUN', 'MON', 'TUE', 'WED', 'THUR'];
    } else if (dayOne === 6) {
        days = ['SAT', 'SUN', 'MON', 'TUE', 'WED', 'THUR', 'FRI'];
    }

    return days;
}

function refineHeatMapData(organisedData, xAxisData, yAxisData) {

    var heatMapData = [];

    for (var i = 0; i < yAxisData.length; ++i) {
        heatMapData.push([]);
    }

    for (var i = 0; i < heatMapData.length; ++i) {
        for (var j = 0; j < xAxisData.length; ++j) {
            heatMapData[i].push(0);
        }

    }

    for (var i = 0; i < organisedData["xAxisData"].length; ++i) {
        var currentDate = organisedData["xAxisData"][i];
        var day = currentDate.getDay();
        var hour = currentDate.getHours();

        heatMapData[day][hour] = organisedData['yAxisData'][0][i];

    }

    return heatMapData;
}

function refineContourMapData(organisedData, nodalData) {

    var refinedData = [];

    for (var i = 0; i < nodalData["nodes"].length; ++i) {

        var currentNode = {};
        currentNode["lat"] = nodalData["nodes"][i]["lat"];
        currentNode["lon"] = nodalData["nodes"][i]["lon"];
        currentNode["value"] = 0;

        for (var j = 0; j < organisedData["yAxisData"].length; ++j) {

            if (organisedData["yAxisData"][j][0] === nodalData["nodes"][i]["nodeID"]) {

                currentNode["value"] = organisedData["yAxisData"][j][1]

            }
            
        }

        refinedData.push(currentNode);
    }

    return refinedData;
}

function drawMap(div) {

    var map = L.map(div, { zoomControl: false }).setView([-27.499, 153.017], 17);

    L.tileLayer('https://api.tiles.mapbox.com/v4/xaellia.pblg62p6/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoieGFlbGxpYSIsImEiOiJjaWxicm8xbGYxamNldWdrbmZuaWE2bXRyIn0.g_PnYml4Xiw7-SFPKHL9bg',
    {
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      subdomains: ['a','b','c','d'],
      mapId: 'xaellia.pblg62p6',
      token: 'pk.eyJ1IjoieGFlbGxpYSIsImEiOiJjaWxicm8xbGYxamNldWdrbmZuaWE2bXRyIn0.g_PnYml4Xiw7-SFPKHL9bg'
    }).addTo(map);

    map.dragging.enable();
    map.touchZoom.disable();
    map.doubleClickZoom.disable();
    map.scrollWheelZoom.disable();
    map.keyboard.disable();

    map._initPathRoot()
    var svg = d3.select(map.getPanes().overlayPane).append("svg");
    var g = svg.append("g").attr("class", "leaflet-zoom-hide");

    return map
}


function drawContours(div, mymap, organisedData, nodalData) {
    var mapWidth = document.getElementById(div).offsetWidth;
    var mapHeight = document.getElementById(div).offsetHeight;

    var data = [];
    var xs = [0];
    var ys = [0];
    var minimumDataValue = 100;

    var refinedData = refineContourMapData(organisedData, nodalData);

    for (var i = 0; i < refinedData.length; ++i) {
        var point = mymap.latLngToLayerPoint(
            L.latLng(parseFloat(refinedData[i]["lat"]), parseFloat(refinedData[i]["lon"])));
        xs.push(point.x);
        ys.push(point.y);
        if (minimumDataValue > parseFloat(refinedData[i]["value"])) {
            minimumDataValue = parseFloat(refinedData[i]["value"]);
        }
    }

    xs.push(mapWidth);
    ys.push(mapHeight);

    xs.sort(function(xs, b) {
        return xs - b;
    });

    ys.sort(function(ys, b) {
        return ys - b;
    });

    var tooCloseToRight = (xs[xs.length - 1] - xs[xs.length - 2]) < 50;
    var tooCloseToLeft = (xs[1] - xs[0]) < 50;
    var tooCloseToTop = (ys[ys.length - 1] - ys[ys.length - 2]) < 50;
    var tooCloseToBottom = (ys[1] - ys[0]) < 50;

    if (!tooCloseToRight) {
        xs.push(xs[xs.length - 2] + 50);
    }

    if (!tooCloseToLeft) {
        xs.push(xs[1] - 50);
    }

    if (!tooCloseToBottom) {
        ys.push(ys[ys.length - 2] + 50);
    }

    if (!tooCloseToTop) {
        ys.push(ys[1] - 50);
    }
  
    xs.sort(function(xs, b) {
        return xs - b;
    });
  
    ys.sort(function(ys, b) {
        return ys - b;
    });

    for (var i = 0; i < xs.length; ++i) {
    
        var row = [];
    
        for (var j = 0; j < ys.length; ++j) {
      
            if (j != 0 && j != (ys.length - 1) && i != 0 && i != (xs.length - 1)) {
        
                if ((!tooCloseToLeft && i == 1) || (!tooCloseToRight && i == (xs.length - 2)) ||
                        (!tooCloseToTop && j == 1) || (!tooCloseToBottom && j == (ys.length - 2))) {
                    row.push(0);
                } else {
                    row.push(minimumDataValue - (minimumDataValue / 10));
                }
            } else {
                row.push(0);
            }
        }
        data.push(row);
    }

    for (var k = 0; k < refinedData.length; ++k) {

        for(var i = 1; i < (xs.length - 1); ++i) {

            var point = mymap.latLngToLayerPoint(
                L.latLng(parseFloat(refinedData[k]["lat"]), parseFloat(refinedData[k]["lon"])));
            

            if (point.x == xs[i]) {
        
                for (var j = 1; j < (ys.length - 1); ++j) {

                    if ((point.y) == ys[j]) {

                        data[i][j] = parseFloat(refinedData[k]["value"]);
                        break;
                    }
                }
            }
        }

    }

    var c = new Conrec;
    var zs = d3.range(0, 15);
    var width = mapWidth;
    var height = mapHeight;
    var x = d3.scale.linear().range([0, width]).domain([0, width]);
    var y = d3.scale.linear().range([0, height]).domain([0, height]);
    var colors = d3.scale.quantile().domain([0, 15]).range([
        "rgba(0, 0, 255, 0)",
        "rgba(26, 0, 229, 0.1)",
        "rgba(51, 0, 204, 0.2)",
        "rgba(77, 0, 178, 0.3)",
        "rgba(102, 0, 153, 0.4)",
        "rgba(128, 1, 128, 0.5)",
        "rgba(153, 1, 102, 0.6)",
        "rgba(178, 1, 77, 0.7)",
        "rgba(204, 1, 51, 0.8)",
        "rgba(229, 1, 26, 0.9)",
        "rgba(255, 1, 0, 1)"
    ]);

    c.contour(data, 0, xs.length - 1, 0, ys.length - 1, xs, ys, zs.length, zs);

    d3.select("#" + div)
        .select("svg")
        .append("g")
        .style("opacity", .8)
        .style()
        .attr("width", width)
        .attr("height", height)
        .selectAll("path")
        .data(c.contourList())
        .enter()
        .append("path")
        .style("fill", function(d) {
            return colors(d.level);
        })
        // .style("stroke","black")
        .attr("d", d3.svg.line().x(function(d) {
            return x(d.x);
        })
        .y(function(d) {
            return y(d.y);
        })
        )
    ;

}