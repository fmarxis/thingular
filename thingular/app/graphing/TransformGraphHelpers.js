/////////////////////
// Transform Graph //
/////////////////////

function toLine (num) {
    if (num == 0) {
        chart.transform('line');
    } else {
        chart2.transform('line');
    }
}

function toSpline (num) {
    if (num == 0) {
        chart.transform('spline');
    } else {
        chart2.transform('spline');
    }
}

function toBar (num) {
    if (num == 0) {
        chart.transform('bar');
    } else {
        chart2.transform('bar');
    }
}