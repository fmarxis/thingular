/**
*** Arguments
*** 
*** data
***     An array of objects containing all the datas.
***     e.g. [{
***     
***         "success": true,
***         "nodeID": "1",
***         "data": [
***             {
***                 "NO2": "7",
***                 "HUM": "54",
***                 "TCA": "43",
***                 "O2": "4",
***                 "CO2": "211",
***                 "CO": "4",
***                 "batteryLevel": "22",
***                 "nodeID": "1",
***                 "timeStamp": "2016-10-01T17:00:00+1000"
***             },
***             {
***                 ...
***             },
***             ...
***         ]
***     }]
***
*** timePeriod
***     String representing how to aggregate the data.
***     e.g. "Hour" "Day" "Week"
***
*** 
*** sensorsChosen
***     Array of strings representing the sensor(s) to get data from.
***     e.g. ["BAT"]
*** 
*** graphType
***     String representation of the type of graph to be plotted
***     e.g. 'line' or 'spline' or 'bar'
*** 
**/
function drawGraph (data, sensorsChosen, graphType, titlePrepend, chartName) {

    var organisedData = organiseData(data, sensorsChosen, titlePrepend);
    if (organisedData == null) {
        return;
    }

    if (xAxisDataContainsNullOrDuplicates(organisedData["xAxisData"])) {
        return null;
    }

    if (!areGivenArrayLengthsEqual(organisedData["xAxisData"], organisedData["yAxisData"][0])) {
        return null;
    }

    var columns = [organisedData["xAxisData"]];
    columns.push.apply(columns, organisedData["yAxisData"]);

    var chart = c3.generate({
        data: {
            xs: {},
            xFormat: '%Y-%m-%dT%H:%M:%S.%LZ',
            columns: [],
            type: graphType
        },
        axis: {
            x: {
                type: 'timeseries',
                tick: {
                    count: 10,
                    format: '%Y-%m-%dT%H:%M:%S'
                }
            }
        },
        zoom: {
            enabled: true,
            rescale: true,
            extent: [1, 100]
        },
        subchart: {
            show: true
        },
        size: {
            height: 480
        },
        bindto: document.getElementById(chartName),
    });

    return chart;
}

function loadGraph (oldChart, data, sensorsChosen, graphType, titlePrepend) {
    var organisedData = organiseData(data, sensorsChosen, titlePrepend);
    if (organisedData == null) {
        return;
    }

    var columns = [];
    var xs = organisedData["xs"];
    columns.push.apply(columns, organisedData["xAxisData"]);
    columns.push.apply(columns, organisedData["yAxisData"]);

    oldChart.load(
        {
            xs: xs,
            columns:columns
        }
    );

    return chart;
}