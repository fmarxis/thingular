const https = require('https');
const fs = require('fs');
const credentials = {
    key: fs.readFileSync('/etc/letsencrypt/live/th1nk.io/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/th1nk.io/cert.pem'),
    // key: fs.readFileSync('D:\\xampp\\apache\\conf\\blarg.key'),
    // cert: fs.readFileSync('D:\\xampp\\apache\\conf\\blarg.cert'),
};
var app = require('express')();
var server = https.createServer(credentials, app);
var serverPort = 8866;

var io = require('socket.io')(server);

var spawn = require('child_process').spawn;

var python;

app.get('/', function(req, res){
    res.sendFile(__dirname + '/client.html');
});

io.on('connection', function(socket){
    socket.emit('chat message', "You are now connected to Thingular Channel");
    socket.on('room', function(msg){
        socket.join(msg);
        socket.emit("update", "You are in room: " + msg);
        console.log('User joined room' + msg);
    });
    socket.on('update', function(msg){
        var temp = msg.split("@|");
        io.to(temp[0]).emit("update", temp[2]);
        io.to(temp[1]).emit("update", temp[2]);
        socket.disconnect();
        console.log("Update received: " + msg);
    });
    socket.on('close', function(msg){
        socket.disconnect();
        console.log("User disconnected");
    });
    socket.on('sync', function(msg){
        console.log("Sync: " + msg);
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        var request = require('request');
        request({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            url: "https://t.th1nk.io/api/sync",
            method: "POST",
            body: msg
        }, function (error, response, body){
            console.log(body);
        });
    });
    socket.on('chat message', function(msg){
        console.log('User Message: ' + JSON.stringify(msg));
        if (msg[0] == "#") {
            msg = msg.substr(1);
            msg = msg.split('#');
            var program, argu, inputs;
            var hasArg = false;
            if(msg.length == 3) {
                program = msg[0];
                argu = msg[1];
                inputs = msg[2];
            } else {
                inputs = "";
            }
            var execArray = [program];
            if (hasArg == true) {}
            argu = argu.split('!');
            var arrayLength = argu.length;
            for (var i = 0; i < arrayLength; i++) {
                execArray.push(argu[i]);
            }
            var py    = spawn('python',execArray ),
                errorString = '',
                dataString = '';

            py.stdout.on('data', function(data){
                dataString += data.toString();
            });
            py.stderr.on('data', function(data){
                errorString += data.toString();
            });
            py.on('close', function(code) {
                socket.emit('chat message', "stdout: " + dataString);
                socket.emit('chat message', "stderr: " + errorString);
            });
            py.stdin.write(inputs);
            py.stdin.end();
        } else {
            io.emit("chat message", msg);
        }
    });
});

server.listen(serverPort, function(){
    console.log('listening on *:8866');
});
