var fs = require('fs');
var path = require('path');
const https = require('https');
const credentials = {
    key: fs.readFileSync('/etc/letsencrypt/live/th1nk.io/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/th1nk.io/cert.pem'),
    // key: fs.readFileSync('D:\\xampp\\apache\\conf\\blarg.key'),
    // cert: fs.readFileSync('D:\\xampp\\apache\\conf\\blarg.cert'),
};
var server = https.createServer(credentials, onRequest);
var serverPort = 8867;

var io = require('socket.io')(server);
var SSHClient = require('ssh2').Client;

// Load static files into memory
var staticFiles = {};
var basePath = path.join(require.resolve('xterm'), '../..');
[ 'addons/fit/fit.js',
  'dist/xterm.css',
  'dist/xterm.js'
].forEach(function(f) {
  staticFiles['/' + f] = fs.readFileSync(path.join(basePath, f));
});
staticFiles['/'] = fs.readFileSync('term.html');

// Handle static file serving
function onRequest(req, res) {
  var file;
  if (req.method === 'GET' && (file = staticFiles[req.url])) {
    res.writeHead(200, {
      'Content-Type': 'text/'
                      + (/css$/.test(req.url)
                         ? 'css'
                         : (/js$/.test(req.url) ? 'javascript' : 'html'))
    });
    return res.end(file);
  }
  res.writeHead(404);
  res.end();
}

io.on('connection', function(socket) {
  var conn = new SSHClient();
  var header = false;
  var command = "";
  conn.on('ready', function() {
    socket.emit('data', '\r\n*** SSH CONNECTION ESTABLISHED ***\r\n');
    conn.shell(function(err, stream) {
      if (err)
        return socket.emit('data', '\r\n*** SSH SHELL ERROR: ' + err.message + ' ***\r\n');
      stream.write("python\r");
      stream.write("import math\r");
      stream.write("import numpy\r");
      stream.write("import sklearn\r");
      stream.write("import dataInterface as di\r");
      socket.on('data', function(data) {
        command += data;
        if(command.indexOf('\r') > -1) {
          if(command.indexOf("import ") > -1) {
            socket.emit('data', '\r\n*** "import" Detected: Connection is forced to close for security reasons***\r\n');
            conn.end();
            socket.disconnect();
          }
          command = "";
        }
        stream.write(data);
      });
      stream.on('data', function(d) {
        if (!header) {
          if (d.toString().indexOf("Python") > -1) {
            header = true;
            socket.emit('data', d.toString('binary'));
          }
        } else {
          if(d.toString().indexOf("[thingularity@th1nk ~]$") > -1) {
            conn.end();
            socket.disconnect();
          } else {
            socket.emit('data', d.toString('binary'));
          }
        }
      }).on('close', function() {
        conn.end();
        socket.disconnect();
      });
    });
  }).on('close', function() {
    socket.emit('data', '\r\n*** SSH CONNECTION CLOSED ***\r\n');
    conn.end();
    socket.disconnect();
  }).on('error', function(err) {
    socket.emit('data', '\r\n*** SSH CONNECTION ERROR: ' + err.message + ' ***\r\n');
    conn.end();
    socket.disconnect();
  }).connect({
    host: '173.255.209.37',
    username: 'thingularity',
    password: 'Thes1s4801'
  });
});

server.listen(serverPort, function(){
    console.log('listening on *:8867');
});
