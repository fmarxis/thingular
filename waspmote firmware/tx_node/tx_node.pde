#include <WaspXBee802.h>
#include <WaspFrame.h>
#include <WaspStackEEPROM.h>


// define variable
uint8_t rx_error;
uint8_t tx_error;
char BROADCAST_ADDRESS[] = "000000000000FFFF";
uint8_t sourceAddr[2];
char RX_ADDRESS[] = "2001";
char data[100];

// Define the Waspmote ID
char ID[] = "RX_NODE";
uint8_t  panID[2] = {0x48,0x01}; 
char OWN_NETADDRESS[] = "2000";

char welcome[] = "Xbee module ON";
char farewell[] = "Xbee module OFF";
int batteryLevel = 0;
unsigned long	epoch;
timestamp_t   time;

int i = 0;


void setup()
{
  // init USB port
  USB.ON();
  USB.println(F("Complete example (RX node)"));
  
  frame.setID(ID);

  // init XBee 
  xbee802.ON();
  xbee802.getOwnMac();
  USB.printf("%02x", xbee802.sourceMacHigh[0]);
  USB.printf("%02x", xbee802.sourceMacHigh[1]);
  USB.printf("%02x", xbee802.sourceMacHigh[2]);
  USB.printf("%02x", xbee802.sourceMacHigh[3]);
  USB.printf("%02x", xbee802.sourceMacLow[0]);
  USB.printf("%02x", xbee802.sourceMacLow[1]);
  USB.printf("%02x", xbee802.sourceMacLow[2]);
  USB.printf("%02x\n", xbee802.sourceMacLow[3]);
  xbee802.setChannel(0x0C);
  xbee802.setNodeIdentifier( ID );
  xbee802.setPAN(panID);
  xbee802.setOwnNetAddress( OWN_NETADDRESS );
  xbee802.setEncryptionMode(1);
  xbee802.setLinkKey("libelium2015MVXB");
  xbee802.writeValues();
  Utils.setAuthKey("LIBELIUM");
  Utils.setID(ID);
  
  RTC.ON();
  RTC.setTime("16:01:01:02:00:00:00");
  USB.println(F("Time Set"));
  USB.println(RTC.getTime());
  
  stack.initBlockSize(200);
  stack.initStack();
}


void loop()
{
  epoch = RTC.getEpochTime();
  RTC.breakTimeAbsolute( epoch, &time );
  USB.println(RTC.getTime());
  
  // create new frame
  frame.createFrame(ASCII);  
  
  // add frame fields
  batteryLevel = PWR.getBatteryLevel();
  //frame.addSensor(SENSOR_STR, "test_frame");
  frame.addSensor(SENSOR_STR, ID);
  frame.addSensor(SENSOR_STR, time.minute);
  frame.addSensor(SENSOR_STR, time.second);
  frame.addSensor(SENSOR_BAT, batteryLevel); 
  frame.showFrame();
  stack.push(frame.buffer, frame.length);
  
  sprintf(data,"Time:%s\r\n", RTC.getTimestamp());

  xbee802.send( BROADCAST_ADDRESS, welcome);
  xbee802.send( BROADCAST_ADDRESS, data);

  rx_error = 1;
  tx_error = 1;
  frame.length = stack.pop( frame.buffer );
  i = 0;
  while (rx_error!=0 || tx_error!=0){
    USB.println("rxtx");
    USB.println(tx_error, DEC);
    USB.println(rx_error, DEC);
    if(PWR.getBatteryLevel()<10){
      break;
    }
    if(tx_error != 0) {
      // send XBee packet
      tx_error = xbee802.send( RX_ADDRESS, frame.buffer, frame.length );
      // check TX flag
      if(tx_error == 0 )
      {
        USB.println(F("send ok"));
        if(!stack.isEmpty()){
          frame.length = stack.pop( frame.buffer );
          tx_error = 1;
        }
      }
      else 
      {
        USB.print(F("send error - "));
        USB.println(i);
      }
      i++;
      delay(1000);
    }
    if(rx_error !=0) {
       // receive XBee packet (wait message for 10 seconds)
      rx_error = xbee802.receivePacketTimeout( 10000 );
      // check answer  
      if(rx_error == 0 ) 
      {    
        USB.println(F("\n1. New packet received"));
        // Show data stored in '_payload' buffer indicated by '_length'
        USB.print(F("--> Data: "));  
        USB.println( xbee802._payload, xbee802._length);
        USB.println( xbee802._payload[0]);
        USB.println( xbee802._payload[1]);
        USB.println( xbee802._payload[2]);
        // Show data stored in '_payload' buffer indicated by '_length'
        USB.print(F("--> Length: "));  
        USB.println( xbee802._length,DEC);
         // Show data stored in '_payload' buffer indicated by '_length'
        USB.print(F("Source Network Address: "));  
        USB.printHex( xbee802._srcNA[0] );
        USB.printHex( xbee802._srcNA[1] );
        USB.println();    
        USB.println(F("--------------------------------"));
        if (xbee802._srcNA[1] == 0x01 && xbee802._payload[0] == '<') {
          USB.println("From 01");
          stack.push(xbee802._payload, xbee802._length);
        } else {
          rx_error = 1;
        }
      }
      else
      {
        USB.print(F("Error receiving a packet:"));
        USB.println(rx_error,DEC);
      }
    }
    
    USB.println(F("\n----------------------------------"));
  }
  ///////////////////////////////////////////
  // 2. Send packet
  //////////////////////////////////////////
  
  while (time.minute < 5) {
    if( xbee802.available() )
    {
      xbee802.treatData();
      // Keep inside this loop while a new program is being received
      while( xbee802.programming_ON  && !xbee802.checkOtapTimeout() )
      {
        if( xbee802.available() )
        {
          xbee802.treatData();
        }
      }
    }
    epoch = RTC.getEpochTime();
    RTC.breakTimeAbsolute( epoch, &time );
  }
  
  if(PWR.getBatteryLevel()<10){
    
  }
  
  xbee802.send( BROADCAST_ADDRESS, farewell);

  delay(3000);
}



