#include <ModbusMaster485.h>

// Step 1. Includes of the Sensor Board and Communications modules used

#include <WaspSensorGas_v20.h>
#include <WaspXBee802.h>
#include <WaspFrame.h>
#include <WaspStackEEPROM.h>

// Step 2. Variables declaration

char  CONNECTOR_A[] = "TCA";      
char  CONNECTOR_B[] = "HUMB";    
char  CONNECTOR_C[] = "CO2";
char  CONNECTOR_D[] = "NO2";
char  CONNECTOR_E[] = "O3";
char  CONNECTOR_F[] = "CO";  

float connectorAFloatValue; 
float connectorBFloatValue;  
float connectorCFloatValue;    
float connectorDFloatValue;   
float connectorEFloatValue;
float connectorFFloatValue;

int connectorAIntValue;
int connectorBIntValue;
int connectorCIntValue;
int connectorDIntValue;
int connectorEIntValue;
int connectorFIntValue;

int   batteryLevel;
// Addresses
//////////////////////////////////////////
char* sleepTime = "00:00:30:00";
char* powerTime = "00:06:00:00";
char  nodeID[10] = "Node_2";
char OWN_NETADDRESS[] = "2002";
char RX_ADDRESS[] = "2004";//0013A20040F81D87
char sleep_packet[] = "2@SLEEP";
int recv_enabled = 1;
char recv_node1 = '1';
char recv_node2 = '2';
char recv_node3 = '3';
char recv_node4 = '4';
//////////////////////////////////////////
uint8_t rx_error;
uint8_t tx_error;
uint8_t done_error;
char BROADCAST_ADDRESS[] = "000000000000FFFF";
uint8_t sourceAddr[2];
uint8_t  panID[2] = {0x20,0x07};
// define variable
uint8_t error;
char data[100];
char welcome[] = "Xbee Module ON";
char farewell[] = "Xbee Module OFF";
unsigned long   epoch;
timestamp_t   time;
int i = 0;
int recv_sleep = 0;
int sentok = 0;
int new_frame = 0;

void setup() 
{
// Step 4. Communication module to ON

    frame.setID(nodeID);

    xbee802.ON();
    xbee802.getOwnMac();
    USB.printf("%02x", xbee802.sourceMacHigh[0]);
    USB.printf("%02x", xbee802.sourceMacHigh[1]);
    USB.printf("%02x", xbee802.sourceMacHigh[2]);
    USB.printf("%02x\n", xbee802.sourceMacHigh[3]);
    USB.printf("%02x", xbee802.sourceMacLow[0]);
    USB.printf("%02x", xbee802.sourceMacLow[1]);
    USB.printf("%02x", xbee802.sourceMacLow[2]);
    USB.printf("%02x\n", xbee802.sourceMacHigh[3]);
    xbee802.setChannel(0x0C);
    xbee802.setNodeIdentifier(nodeID);
    xbee802.setPAN(panID);
    xbee802.setOwnNetAddress( OWN_NETADDRESS );
    xbee802.setEncryptionMode(1);
    xbee802.setLinkKey("libelium2015MVXB");
    xbee802.writeValues();
    Utils.setAuthKey("LIBELIUM");
    Utils.setID(nodeID);

// Step 7. Communication module to OFF

    xbee802.OFF();
    delay(100);
    USB.println("Initilization Finished");

    RTC.ON();
    RTC.setTime("16:01:01:02:00:00:00");
    USB.println(F("Time Set"));
    USB.println(RTC.getTime());
  
    stack.initBlockSize(200);
    stack.initStack();
    USB.print("Max frame number:");
    USB.println(stack.getMaxFrames(), DEC);
    
    batteryLevel = PWR.getBatteryLevel();
    USB.print("Power:");
    USB.println(batteryLevel);
}

void loop()
{
    RTC.ON();
// Step 8. Turn on the Sensor Board
    
    epoch = RTC.getEpochTime();
    RTC.breakTimeAbsolute( epoch, &time );
    USB.println(RTC.getTime());

    //Turn on the sensor board
    SensorGasv20.ON();
    //supply stabilization delay
    delay(100);

// Step 9. Turn on the sensors

    // First dummy reading for analog-to-digital converter channel selection
    PWR.getBatteryLevel();
    // Getting Battery Level
    batteryLevel = PWR.getBatteryLevel();
    USB.print("Battery Read OK - ");
    USB.println(batteryLevel);
    
    // Configuring NO2 sensor
    delay(1000);    
    //Turning on NO2 Sensor
    SensorGasv20.setSensorMode(SENS_ON, SENS_SOCKET3B);
    delay(30000);

    //Turning on and configuring O3 Sensor
    SensorGasv20.setSensorMode(SENS_ON, SENS_SOCKET2B);
    SensorGasv20.configureSensor(SENS_SOCKET2B, 1, 10);
    delay(30000);
    
    SensorGasv20.configureSensor(SENS_SOCKET4CO, 1, 100);
    
    USB.println("Sensor Configured");
// Step 10. Read the sensors
    //First dummy reading for analog-to-digital converter channel selection
    //Sensor temperature reading
    connectorAFloatValue = SensorGasv20.readValue(SENS_TEMPERATURE);
    //Conversion into a string
    //Utils.float2String(connectorAFloatValue, connectorAString, 2);
    USB.println("Temperature Read OK");
    //First dummy reading for analog-to-digital converter channel selection
    //Sensor temperature reading
    connectorBFloatValue = SensorGasv20.readValue(SENS_HUMIDITY);
    //Conversion into a string
    //Utils.float2String(connectorBFloatValue, connectorBString, 2);
    USB.println("Humidity Read OK");

///////////////////////////////
    ///////////////////////////////
    //First dummy reading to set analog-to-digital channel
    SensorGasv20.configureSensor(SENS_SOCKET3B, 1, 3);
    connectorDFloatValue = SensorGasv20.readValue(SENS_SOCKET3B);    
    //Conversion into a string
    //Utils.float2String(connectorDFloatValue, connectorDString, 2);
    USB.println("NO2 Read OK");
    delay(1000);
    
    ///////////////////////////////
    //First dummy reading to set analog-to-digital channel
    connectorEFloatValue = SensorGasv20.readValue(SENS_SOCKET2B);    
    //Conversion into a string
    //Utils.float2String(connectorEFloatValue, connectorEString, 2);
    USB.println("O3 Read OK");
    delay(1000);
    
    //First dummy reading to set analog-to-digital channel
    connectorFFloatValue = SensorGasv20.readValue(SENS_SOCKET4CO);    
    //Conversion into a string
    //Utils.float2String(connectorFFloatValue, connectorFString, 2);
    USB.println("CO Read OK");
    delay(1000);
    
    //Configure and turn on the CO2 sensor
    SensorGasv20.configureSensor(SENS_CO2, 7);
    SensorGasv20.setSensorMode(SENS_ON, SENS_CO2);
    delay(30000);
    ///////////////////////////////
    //First dummy reading to set analog-to-digital channel
    connectorCFloatValue = SensorGasv20.readValue(SENS_CO2);
    //SensorGasv20.setSensorMode(SENS_OFF, SENS_CO2);     
    //Conversion into a string
    //Utils.float2String(connectorCFloatValue, connectorCString, 2);
    USB.println("CO2 Read OK");
    USB.println(connectorCFloatValue);
    SensorGasv20.setSensorMode(SENS_OFF, SENS_CO2);
///////////////////////////////
// Step 11. Turn off the sensors

    //En el caso de la placa de eventos no aplica

    SensorGasv20.setSensorMode(SENS_OFF, SENS_SOCKET3B);

    SensorGasv20.setSensorMode(SENS_OFF, SENS_SOCKET2B);

// Step 12. Message composition

// Step 13. Communication module to ON
    
    sprintf(data,"Time:%s\r\n", RTC.getTimestamp());

    USB.println(welcome);
    USB.println(data);
    
    // create new frame
    frame.createFrame(ASCII);  
    
    // add frame fields
    frame.addSensor(SENSOR_STR, nodeID);
    frame.addSensor(SENSOR_BAT, batteryLevel);
    frame.addSensor(SENSOR_TCA, connectorAFloatValue);
    frame.addSensor(SENSOR_HUMB, connectorBFloatValue);
    frame.showFrame();
    stack.push(frame.buffer, frame.length);
    
    // create new frame
    frame.createFrame(ASCII);  
    
    // add frame fields
    frame.addSensor(SENSOR_CO2, connectorCFloatValue);
    frame.addSensor(SENSOR_NO2, connectorDFloatValue);
    frame.addSensor(SENSOR_O3, connectorEFloatValue);
    frame.addSensor(SENSOR_CO, connectorFFloatValue);
    frame.showFrame();
    stack.push(frame.buffer, frame.length);
    ///////////////////////////////////////////
    // 2. Send packet
    /////////////////////////////////////////// 
    rx_error = recv_enabled; 
    tx_error = 1;
    done_error = 1;
    recv_sleep = 0;
    sentok = 0;
    new_frame = 1;
    i = 0;
    
    xbee802.ON();
    while (rx_error!=0 || tx_error!=0 || done_error!=0){
      USB.print("Starting Transmission");
      USB.print("Number of stored frames:");
      USB.println(stack.getStoredFrames());
      USB.println("==========================================");
      USB.println(tx_error, DEC);
      USB.println(rx_error, DEC);
      if(PWR.getBatteryLevel()<10){
        break;
      }
      if(rx_error == 0 && tx_error != 0) {
        // send XBee packet
        if (new_frame == 1) {
          frame.length = stack.pop( frame.buffer );
          new_frame = 0;
        }
        USB.print("Data to send:");
        USB.println(frame.buffer, frame.length);
        tx_error = xbee802.send( RX_ADDRESS, frame.buffer, frame.length );
        // check TX flag
        if(tx_error == 0)
        {
          USB.println(F("Send OK"));
          if(!stack.isEmpty()){
            frame.length = stack.pop( frame.buffer );
            tx_error = 1;
            USB.println(F("Stack not empty - keep sending..."));
          } else {
            USB.println(F("Sent Finished"));
            if (sentok == 0) {
              sentok = 1;
            }
          }
          delay(5000);
        }
        else 
        {
          USB.print(F("send error - "));
          USB.println(i);
        }
        i++;
        delay(3000);
      }
      if(rx_error !=0) {
         // receive XBee packet (wait message for 10 seconds)
        rx_error = xbee802.receivePacketTimeout( 10000 );
        // check answer  
        if(rx_error == 0) 
        {    
          USB.println(F("\n1. New packet received"));
          // Show data stored in '_payload' buffer indicated by '_length'
          USB.print(F("--> Data: "));
          USB.println( xbee802._payload, xbee802._length);
          // Show data stored in '_payload' buffer indicated by '_length'
          USB.print(F("--> Length: "));
          USB.println( xbee802._length,DEC);
           // Show data stored in '_payload' buffer indicated by '_length'
          USB.print(F("Source Network Address: "));  
          USB.printHex( xbee802._srcNA[0] );
          USB.printHex( xbee802._srcNA[1] );
          USB.println();    
          USB.println(F("----------------------------------"));
          if (xbee802._payload[0] == '<' && xbee802._payload[21] == '2') {
            USB.print("Data From - ");
            USB.println(recv_node2);
            stack.push(xbee802._payload, xbee802._length);
            new_frame = 1;
            tx_error = 1;
          } else if (xbee802._payload[0] == '<' && xbee802._payload[21] == '3') {
            USB.print("Data From - ");
            USB.println(recv_node3);
            stack.push(xbee802._payload, xbee802._length);
            new_frame = 1;
            tx_error = 1;
          } else if (xbee802._payload[0] == '<' && xbee802._payload[21] == '4') {
            USB.print("Data From - ");
            USB.println(recv_node4);
            stack.push(xbee802._payload, xbee802._length);
            new_frame = 1;
            tx_error = 1;
          } else if (xbee802._payload[1] == '@' && xbee802._payload[0] == '3' && xbee802._payload[2] == 'S'){
            USB.print("Sleep From - ");
            USB.println(recv_node3);
            recv_sleep = 1;
          }
          if(recv_sleep == 1) {
            rx_error = 0;
          } else {
            rx_error = 1;
          }
        }
        else
        {
          USB.print(F("Error receiving a packet:"));
          USB.println(rx_error,DEC);
        }
      }
      if(done_error != 0 && tx_error == 0 && rx_error == 0 && sentok == 1) {
        delay(5000);
        done_error = xbee802.send( RX_ADDRESS, sleep_packet);
        // check TX flag
        if(done_error == 0)
        {
          USB.println(F("Sleep Send OK"));
        }
        else 
        {
          USB.print(F("send error - "));
          USB.println(i);
        }
        i++;\
      }
      USB.println(F("=========================================="));
    }
    while (time.minute > 55) {
      if( xbee802.available() )
      {
        xbee802.treatData();
        // Keep inside this loop while a new program is being received
        while( xbee802.programming_ON  && !xbee802.checkOtapTimeout() )
        {
          if( xbee802.available() )
          {
            xbee802.treatData();
          }
        }
      }
      epoch = RTC.getEpochTime();
      RTC.breakTimeAbsolute( epoch, &time );
      if(PWR.getBatteryLevel()<10){
        break;
      }
    }
    
    if(PWR.getBatteryLevel()<10){
      PWR.deepSleep(powerTime,RTC_OFFSET,RTC_ALM1_MODE1,ALL_OFF);
    }
    
    USB.println(farewell);

// Step 15. Communication module to OFF

    xbee802.OFF();
    delay(100);
    USB.println(F("Entering sleep for 30 minutes..."));
// Step 16. Entering Sleep Mode
    PWR.deepSleep(sleepTime,RTC_OFFSET,RTC_ALM1_MODE1,ALL_OFF);
    USB.println(F("Woken up"));
}
